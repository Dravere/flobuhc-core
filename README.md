# FLoB-UHC-Core Spigot MC Plugin

This is a simple UHC plugin for [Spigot](https://www.spigotmc.org/) and [Paper](https://aquifermc.org/) Minecraft servers. It was developed as part of the [Far Lands or Bust](http://farlandsorbust.com/) community (FLoB).
 
## Installation
 
Just drop it in the `plugins` folder of your server. After the first start of the server an additional folder should created below your plugin folder with the name of this plugin. In that folder you'll find a config.yml with the default configuration for the plugin.

The entire configuration can be changed in-game through the sub-command `config`. After starting the game the configuration will be saved so it can be reused in the next game.
 
## Configuration

These are the available configuration in version 0.7.0-SNAPSHOT:

| Path | Type | Description | Example |
|------|------|-------------|---------|
| game.spread.keepTeams | Boolean | Should the teams be kept together while spreading the players. | true |
| game.spread.minDistance | Double | Minimum distance in blocks that spread players/teams should have to each other. You can provide NaN and it will try to determine an useful number in regards to amount of teams/players and play area size. | 320 |
| game.area.start.world | String | The name of the world the game should start in. | lobby |
| game.area.start.centerX | Double | The center of the starting area on the X axis. | 0.0 |
| game.area.start.centerY | Double | The center of the starting area on the Y axis. This can be NaN and the highest block with direct sunlight will be used. | NaN |
| game.area.start.centerZ | Double | The center of the starting area on the Z axis. | 0.0 |
| game.area.start.radius | Double | The border radius (square) for the starting area. | 32.0 |
| game.area.training.enabled | Boolean | If the training grounds should be active or not. They can only be used during game preparations. | false |
| game.area.training.world | String | The world name the training grounds are located. This has to be a different world than the starting world. But it can be the same as the playing world. | world |
| game.area.training.centerX | Double | The center of the training area on the X axis. | 0.0 |
| game.area.training.centerZ | Double | The center of the training area on the Z axis. | 0.0 |
| game.area.training.radius | Double | The border radius (square) for the training area. | 100.0 |
| game.area.play.cageY | Double | Players going below the given Y value will be teleported back to the center. Can be disabled with the value NaN. | 140 |
| game.area.play.world | String | The name of the world the game should play in. | world |
| game.area.play.centerX | Double | The center of the playing area on the X axis. | 0.0 |
| game.area.play.centerY | Double | The center of the playing area on the Y axis. This can be NaN and the highest block with direct sunlight will be used. | NaN |
| game.area.play.centerZ | Double | The center of the playing area on the Z axis. | 0.0 |
| game.area.play.radius | Double | The radius in blocks for the playing area. Keep in mind that the area is a square and not a circle. Thus this is half the length of an edge. | 1000 |
| game.area.play.cageY | Double | Players going below the given Y value will be teleported back to the center. Can be disabled with the value NaN. | NaN |
| game.area.shrink.enabled | Boolean | Set to true if the border should shrink over time. | true |
| game.area.shrink.radius | Double | The radius (square) the border should shrink to. | 100.0 |
| game.area.shrink.time | Long | The time in **minutes** it takes for the border to shrink to the set value. | 120 |
| game.area.shrink.holdOff | Long | The time in **minutes** before the border starts shrinking. | 5 |
| game.timer.episode | Long | The time in **minutes** for episode cut markers to be announced. Can be turned off by setting the value to 0. | 20 |
| game.combat.holdOff | Long | The time in **minutes** before PVP is enabled | 10 |
| game.daylightCycle.start.enabled | Boolean | Set to true if there should be a daylight cycle while starting. | false |
| game.daylightCycle.start.time | Long | Set the time of day in ticks that will be set after create is called. | 1000 |
| game.daylightCycle.play.enabled | Boolean | Set to true if there should be a daylight cycle while playing. | true |
| game.daylightCycle.play.time | Long | Set the time of day in ticks that will be set after ready and start are called. | 1000 |
| game.daylightCycle.finish.enabled | Boolean | Set to true if there should be a daylight cycle while starting. | false |
| game.daylightCycle.finish.time | Long | Set the time of day in ticks that will be set after finish is called. | 12000 |
| game.spectating.viewPlayerInventory | Boolean | Set to true so that spectators can right click other players to see their inventory. |
| game.fireworks.enabled | Boolean | If enabled a firework rocket will fire regularly at the players position at the highest block that has an unobstructed sight to the sky. |
| game.fireworks.power | Long | The power of the firework rockets (typically in-game they are from 1 to 3). Maximum is 128. |
| game.fireworks.holdOff | Long | The time in **minutes** before fireworks are being fired for the first time. |
| game.fireworks.every | Long | The time in **minutes** between each firing of fireworks. |
| compatibility.multiVerse | Boolean | If true it will give the permission to every player joining the game to bypass game mode requirements of MultiVerse. This is needed for spectator mode to work with MultiVerse. | true |

## Commands

These are the available commands in version 0.7.0-SNAPSHOT. They all start with `/flobuhc <sub-command> [parameters]`

| Sub-Cmd | Parameters | Permission | Description |
|---------|------------|------------|-------------|
| info | | flobuhc.operator | Displays the name and version of the plugin. Also a link to the code is given. This also works if no sub-command is given. |
| config list | | flobuhc.operator | Lists all the current configuration paths and their values. |
| config get | path | flobuhc.operator | Displays the current value of the given path. |
| config set | path value | flobuhc.operator | Set the configuration for the given path to the given value. |
| create | | flobuhc.operator | Setups the starting area for the game and teleports everyone there. |
| ready | | flobuhc.operator | Spreads the players in the game area. Gives blindness and slowness to everyone. |
| start | | flobuhc.operator | Starts the game and timers. Clears the inventory of every player. |
| finish | | flobuhc.operator | Resets the border to the gaming area. Sets everyone into spectator mode. |
| exit | | flobuhc.operator | Exits the UHC game mode. |
| join | name | flobuhc.player | The player joins the team specified by the name. If the team doesn't exist yet, it will be created and a random color assigned. |
| team | team-name add/color player-name/color-name | flobuhc.player | As operator or with flobuhc.operator permissions you can add a player to a team. Also possible is changing the color of your team or of every team as operator. |
| spectate | | flobuhc.player | The player joins the spectators and will spectate when the game starts. This has to be issued after create and before ready. |
| nightVision | | flobuhc.player | Can only be used by a spectator to toggle night vision on and off. |
| revive | player | flobuhc.operator | A dead player can be tagged to be alive again. The player will need to re-log. Nothing else is done. |
| training start | | flobuhc.player | If training is activated the player is teleported to the training grounds and equipped with basic combat equipment. |
| training exit | | flobuhc.player | If a player is in the training grounds he can exit them with this command. |
| training help | | flobuhc.player | Displays a short help on how to use the training grounds command. |
