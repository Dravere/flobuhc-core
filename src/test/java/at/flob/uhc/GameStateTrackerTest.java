package at.flob.uhc;


import org.junit.Test;

import static org.junit.Assert.*;


public class GameStateTrackerTest
{
    @Test
    public void testConstructor()
    {
        GameStateTracker tracker = new GameStateTracker();
        assertEquals(tracker.getCurrentState(), GameState.STANDBY);
    }

    @Test
    public void testStandbyState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertFalse(tracker.tryChangeState(GameState.READY));
        assertFalse(tracker.tryChangeState(GameState.RUNNING));
        assertFalse(tracker.tryChangeState(GameState.FINISHED));
        assertEquals(tracker.getCurrentState(), GameState.STANDBY);
        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertEquals(tracker.getCurrentState(), GameState.CREATED);
    }

    @Test
    public void testCreatedState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertEquals(tracker.getCurrentState(), GameState.CREATED);

        assertFalse(tracker.tryChangeState(GameState.RUNNING));
        assertFalse(tracker.tryChangeState(GameState.FINISHED));
        assertEquals(tracker.getCurrentState(), GameState.CREATED);
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertEquals(tracker.getCurrentState(), GameState.READY);
    }

    @Test
    public void testExitCreatedState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertEquals(tracker.getCurrentState(), GameState.CREATED);
        assertTrue(tracker.tryChangeState(GameState.STANDBY));
        assertEquals(tracker.getCurrentState(), GameState.STANDBY);
    }

    @Test
    public void testReadyState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertEquals(tracker.getCurrentState(), GameState.READY);

        assertFalse(tracker.tryChangeState(GameState.FINISHED));
        assertEquals(tracker.getCurrentState(), GameState.READY);
        assertTrue(tracker.tryChangeState(GameState.RUNNING));
        assertEquals(tracker.getCurrentState(), GameState.RUNNING);
    }

    @Test
    public void testExitReadyState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertEquals(tracker.getCurrentState(), GameState.READY);

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertEquals(tracker.getCurrentState(), GameState.CREATED);
    }

    @Test
    public void testExitReadyStateToStandby()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertEquals(tracker.getCurrentState(), GameState.READY);

        assertTrue(tracker.tryChangeState(GameState.STANDBY));
        assertEquals(tracker.getCurrentState(), GameState.STANDBY);
    }

    @Test
    public void testRunningState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertTrue(tracker.tryChangeState(GameState.RUNNING));
        assertEquals(tracker.getCurrentState(), GameState.RUNNING);

        assertFalse(tracker.tryChangeState(GameState.STANDBY));
        assertFalse(tracker.tryChangeState(GameState.CREATED));
        assertFalse(tracker.tryChangeState(GameState.READY));
        assertEquals(tracker.getCurrentState(), GameState.RUNNING);
        assertTrue(tracker.tryChangeState(GameState.FINISHED));
        assertEquals(tracker.getCurrentState(), GameState.FINISHED);
    }

    @Test
    public void testFinishedState()
    {
        GameStateTracker tracker = new GameStateTracker();

        assertTrue(tracker.tryChangeState(GameState.CREATED));
        assertTrue(tracker.tryChangeState(GameState.READY));
        assertTrue(tracker.tryChangeState(GameState.RUNNING));
        assertTrue(tracker.tryChangeState(GameState.FINISHED));
        assertEquals(tracker.getCurrentState(), GameState.FINISHED);

        assertFalse(tracker.tryChangeState(GameState.CREATED));
        assertFalse(tracker.tryChangeState(GameState.READY));
        assertFalse(tracker.tryChangeState(GameState.RUNNING));
        assertEquals(tracker.getCurrentState(), GameState.FINISHED);
        assertTrue(tracker.tryChangeState(GameState.STANDBY));
        assertEquals(tracker.getCurrentState(), GameState.STANDBY);
    }
}