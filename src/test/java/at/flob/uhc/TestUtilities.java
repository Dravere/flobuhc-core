// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


/**
 * @author Florian Graf
 */
public class TestUtilities
{
    private static ItemStack createItemStack(Material material)
    {
        ItemStack item = mock(ItemStack.class);
        when(item.getType()).thenReturn(material);
        return item;
    }

    private static ItemStack createItemStack(PotionType potionType)
    {
        ItemStack potion = createItemStack(Material.POTION);
        PotionMeta potionMeta = mock(PotionMeta.class);
        when(potionMeta.getBasePotionData()).thenReturn(new PotionData(potionType, false, false));
        when(potion.getItemMeta()).thenReturn(potionMeta);
        return potion;
    }

    public static Player createPlayer()
    {
        return createPlayer(new ArrayList<>());
    }

    public static Player createPlayer(ArrayList<String> messages)
    {
        Player player = mock(Player.class);
        doAnswer(invocation ->
        {
            messages.add(invocation.getArgument(0).toString());
            return null;
        }).when(player).sendMessage(any(String.class));

        final GameMode[] mode = new GameMode[] { GameMode.SURVIVAL };
        doAnswer(invocation -> mode[0] = invocation.getArgument(0)).when(player).setGameMode(any(GameMode.class));
        when(player.getGameMode()).thenAnswer(invocation -> mode[0]);

        Set<String> tags = new HashSet<>();
        doAnswer(invocation -> tags.add(invocation.getArgument(0))).when(player).addScoreboardTag(any(String.class));
        when(player.getScoreboardTags()).thenReturn(tags);

        when(player.removeScoreboardTag(any(String.class)))
            .thenAnswer(invocation -> tags.remove(invocation.getArgument(0).toString()));

        doAnswer(invocation -> null).when(player).setBedSpawnLocation(any(Location.class), any(Boolean.class));

        when(player.getUniqueId()).thenReturn(UUID.randomUUID());

        return player;
    }

    public static BrewEvent createBrewEvent(Player player, Material ingredientMaterial, PotionType potionType)
    {
        ItemStack ingredient = createItemStack(ingredientMaterial);
        ItemStack[] potions = new ItemStack[] { createItemStack(potionType) };

        BrewerInventory inventory = mock(BrewerInventory.class);
        when(inventory.getViewers()).thenReturn(Collections.singletonList(player));
        when(inventory.getIngredient()).thenReturn(ingredient);
        when(inventory.getContents()).thenReturn(potions);

        return new BrewEvent(null, inventory, 100);
    }

    public static PluginConfiguration getConfiguration()
    {
        Plugin plugin = mock(Plugin.class);

        YamlConfiguration yamlConfiguration = new YamlConfiguration();

        try
        {
            yamlConfiguration.load(TestUtilities.class.getClassLoader().getResource("config.yml").getFile());
        }
        catch(Exception exception)
        {
            throw new RuntimeException(exception);
        }

        when(plugin.getConfig()).thenReturn(yamlConfiguration);

        return new PluginConfiguration(plugin);
    }
}
