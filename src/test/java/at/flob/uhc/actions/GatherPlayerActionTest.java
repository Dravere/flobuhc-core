package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControlTestAdapter;
import at.flob.uhc.TestUtilities;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


/**
 * @author Florian Graf
 */
public class GatherPlayerActionTest
{
    private static Player createPlayer()
    {
        Location location = new Location(null, -100, 100, 200);
        Player player = mock(Player.class);
        doAnswer(invocation -> {
            Location target = invocation.getArgument(0);
            location.setWorld(target.getWorld());
            location.setX(target.getX());
            location.setY(target.getY());
            location.setZ(target.getZ());
            return null;
        }).when(player).teleport(any(Location.class));
        when(player.getLocation()).thenReturn(location);
        return player;
    }

    @Test
    public void testPlayerGatheringOnTargetState()
    {
        Player player = createPlayer();
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        serverControl.addPlayer(player);

        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(new GatherPlayerAction(serverControl, GameState.CREATED));

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());
        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        assertEquals(0, player.getLocation().getX(), 0.0001);
        assertEquals(128, player.getLocation().getY(), 0.0001);
        assertEquals(0, player.getLocation().getZ(), 0.0001);
    }

    @Test
    public void testPlayerGatheringOnDifferentState()
    {
        Player player = createPlayer();
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        serverControl.addPlayer(player);

        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(new GatherPlayerAction(serverControl, GameState.READY));

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());
        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        assertEquals(-100, player.getLocation().getX(), 0.0001);
        assertEquals(100, player.getLocation().getY(), 0.0001);
        assertEquals(200, player.getLocation().getZ(), 0.0001);
    }
}