package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControlTestAdapter;
import at.flob.uhc.TestUtilities;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class BorderConfigurationTest
{
    @Test
    public void testCreatedBorder()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        serverControl.createWorld("Telara");
        BorderConfiguration configuration = new BorderConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        assertEquals(0, serverControl.getBorderCenterX(), 0.0001);
        assertEquals(0, serverControl.getBorderCenterZ(), 0.0001);
        assertEquals(32, serverControl.getBorderSize(), 0.0001);
    }

    @Test
    public void testStandbyBorder()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        serverControl.createWorld("Telara");
        BorderConfiguration configuration = new BorderConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.STANDBY));

        assertEquals(0, serverControl.getBorderCenterX(), 0.0001);
        assertEquals(0, serverControl.getBorderCenterZ(), 0.0001);
        assertEquals(30000000, serverControl.getBorderSize(), 0.0001);
    }

    @Test
    public void testReadyBorder()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        serverControl.createWorld("world");
        serverControl.createWorld("Telara");
        BorderConfiguration configuration = new BorderConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.READY));

        assertEquals(0, serverControl.getBorderCenterX(), 0.0001);
        assertEquals(0, serverControl.getBorderCenterZ(), 0.0001);
        assertEquals(2000, serverControl.getBorderSize(), 0.0001);
    }
}