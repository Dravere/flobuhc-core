package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControlTestAdapter;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class HealthRegenerationConfigurationTest
{
    @Test
    public void testHealthRegenIsOn()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        HealthRegenerationConfiguration healthRegenerationConfiguration = new HealthRegenerationConfiguration(serverControl);
        gameStateTracker.addGameStateListener(healthRegenerationConfiguration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(serverControl.isHealthRegenerationEnabled());

        assertTrue(gameStateTracker.tryChangeState(GameState.STANDBY));
        assertTrue(serverControl.isHealthRegenerationEnabled());
    }

    @Test
    public void testHealthRegenIsOff()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        HealthRegenerationConfiguration healthRegenerationConfiguration = new HealthRegenerationConfiguration(serverControl);
        gameStateTracker.addGameStateListener(healthRegenerationConfiguration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(serverControl.isHealthRegenerationEnabled());

        assertTrue(gameStateTracker.tryChangeState(GameState.READY));
        assertTrue(serverControl.isHealthRegenerationEnabled());

        assertTrue(gameStateTracker.tryChangeState(GameState.RUNNING));
        assertFalse(serverControl.isHealthRegenerationEnabled());

        assertTrue(gameStateTracker.tryChangeState(GameState.FINISHED));
        assertTrue(serverControl.isHealthRegenerationEnabled());
    }
}