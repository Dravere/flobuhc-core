package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControlTestAdapter;
import at.flob.uhc.TestUtilities;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author Florian Graf
 */
public class GameModeConfigurationTest
{
    @Test
    public void testStandbyJoinPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerJoinEvent(player, ""));
        assertEquals(GameMode.SURVIVAL, player.getGameMode());
    }

    @Test
    public void testStandbyRespawnPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerRespawnEvent(player, new Location(null, 0, 0, 0), false));
        assertEquals(GameMode.SURVIVAL, player.getGameMode());
    }

    @Test
    public void testCreatedOnlinePlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        Player one = TestUtilities.createPlayer();
        serverControl.addPlayer(one);

        Player two = TestUtilities.createPlayer();
        serverControl.addPlayer(two);

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        assertEquals(GameMode.ADVENTURE, one.getGameMode());
        assertEquals(GameMode.ADVENTURE, two.getGameMode());
    }

    @Test
    public void testCreatedJoinPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerJoinEvent(player, ""));
        assertEquals(player.getGameMode(), GameMode.ADVENTURE);
    }

    @Test
    public void testCreatedRespawnPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerRespawnEvent(player, new Location(null, 0, 0, 0), false));
        assertEquals(GameMode.ADVENTURE, player.getGameMode());
    }

    @Test
    public void testReadyOnlinePlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        Player one = TestUtilities.createPlayer();
        serverControl.addPlayer(one);

        Player two = TestUtilities.createPlayer();
        serverControl.addPlayer(two);

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.READY));

        assertEquals(GameMode.ADVENTURE, one.getGameMode());
        assertEquals(GameMode.ADVENTURE, two.getGameMode());
    }

    @Test
    public void testReadyJoinPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.READY));

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerJoinEvent(player, ""));
        assertEquals(GameMode.SPECTATOR, player.getGameMode());
    }

    @Test
    public void testReadyReJoinPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        Player player = TestUtilities.createPlayer();
        serverControl.addPlayer(player);

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.READY));

        assertEquals(GameMode.ADVENTURE, player.getGameMode());
        serverControl.fireEvent(new PlayerJoinEvent(player, ""));
        assertEquals(GameMode.ADVENTURE, player.getGameMode());
    }

//    @Test
//    public void testReadyRespawnPlayers()
//    {
//        GameStateTracker gameStateTracker = new GameStateTracker();
//        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());
//
//        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
//        Player player = TestUtilities.createPlayer();
//        serverControl.addPlayer(player);
//
//        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
//        gameStateTracker.addGameStateListener(configuration);
//
//        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
//        assertTrue(gameStateTracker.tryChangeState(GameState.READY));
//        TODO Can't test because world.getBlock is not implemented!
//        assertEquals(GameMode.SURVIVAL, player.getGameMode());
//        serverControl.fireEvent(new PlayerDeathEvent(player, new ArrayList<>(), 0, ""));
//        serverControl.fireEvent(new PlayerRespawnEvent(player, new Location(null, 0, 0, 0), false));
//        assertEquals(GameMode.SPECTATOR, player.getGameMode());
//    }

    @Test
    public void testCreatedExitOnlinePlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        Player one = TestUtilities.createPlayer();
        serverControl.addPlayer(one);

        Player two = TestUtilities.createPlayer();
        serverControl.addPlayer(two);

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));

        assertEquals(GameMode.ADVENTURE, one.getGameMode());
        assertEquals(GameMode.ADVENTURE, two.getGameMode());

        assertTrue(gameStateTracker.tryChangeState(GameState.STANDBY));

        assertEquals(GameMode.SURVIVAL, one.getGameMode());
        assertEquals(GameMode.SURVIVAL, two.getGameMode());
    }

    @Test
    public void testCreatedExitJoinPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.STANDBY));

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerJoinEvent(player, ""));
        assertEquals(GameMode.SURVIVAL, player.getGameMode());
    }

    @Test
    public void testCreatedExitRespawnPlayers()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();

        GameModeConfiguration configuration = new GameModeConfiguration(serverControl);
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertTrue(gameStateTracker.tryChangeState(GameState.STANDBY));

        Player player = TestUtilities.createPlayer();
        serverControl.fireEvent(new PlayerRespawnEvent(player, new Location(null, 0, 0, 0), false));
        assertEquals(GameMode.SURVIVAL, player.getGameMode());
    }
}