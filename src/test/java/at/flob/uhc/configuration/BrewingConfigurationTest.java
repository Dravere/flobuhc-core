package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControlTestAdapter;
import at.flob.uhc.TestUtilities;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.potion.PotionType;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class BrewingConfigurationTest
{
    @Test
    public void testStandbyBrewingRegen()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.GHAST_TEAR, PotionType.AWKWARD);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }

    @Test
    public void testStandbyBrewingStrengthTwo()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.GLOWSTONE_DUST, PotionType.STRENGTH);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }

    @Test
    public void testStandbyBrewingAwkward()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.NETHER_WARTS, PotionType.WATER);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }

    @Test
    public void testStandbyBrewingPoison()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.SPIDER_EYE, PotionType.AWKWARD);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }

    @Test
    public void testCreatedBrewingRegen()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertEquals(GameState.CREATED, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.GHAST_TEAR, PotionType.AWKWARD);
        serverControl.fireEvent(event);

        assertTrue(event.isCancelled());
        assertEquals(1, messages.size());
    }

    @Test
    public void testCreatedBrewingStrengthTwo()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertEquals(GameState.CREATED, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.GLOWSTONE_DUST, PotionType.STRENGTH);
        serverControl.fireEvent(event);

        assertTrue(event.isCancelled());
        assertEquals(1, messages.size());
    }

    @Test
    public void testCreatedBrewingAwkward()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertEquals(GameState.CREATED, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.NETHER_WARTS, PotionType.WATER);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }

    @Test
    public void testCreatedBrewingPoison()
    {
        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
        BrewingConfiguration configuration = new BrewingConfiguration(serverControl);
        GameStateTracker gameStateTracker = new GameStateTracker();
        gameStateTracker.addGameStateListener(configuration);

        assertTrue(gameStateTracker.tryChangeState(GameState.CREATED));
        assertEquals(GameState.CREATED, gameStateTracker.getCurrentState());

        ArrayList<String> messages = new ArrayList<>();
        Player player = TestUtilities.createPlayer(messages);

        BrewEvent event = TestUtilities.createBrewEvent(player, Material.SPIDER_EYE, PotionType.AWKWARD);
        serverControl.fireEvent(event);

        assertFalse(event.isCancelled());
        assertEquals(0, messages.size());
    }
}