package at.flob.uhc;


import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class MinecraftCommandGeneratorTest
{
    @Test
    public void testSpreadPlayersCommand()
    {
        assertEquals(
            "spreadplayers 0 0 500 1000 true @a",
            MinecraftCommandGenerator.createSpreadPlayersCommand(
                0,
                0,
                1000,
                500,
                true));
    }
}