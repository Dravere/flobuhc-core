package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class GameStateChangeCommandTest
{
    @Test
    public void testNormalGameStateChanges()
    {
        GameStateTracker gameStateTracker = new GameStateTracker();
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());

        new GameStateChangeCommand(gameStateTracker, GameState.CREATED).execute(null, CommandArguments.NONE);
        assertEquals(GameState.CREATED, gameStateTracker.getCurrentState());

        new GameStateChangeCommand(gameStateTracker, GameState.READY).execute(null, CommandArguments.NONE);
        assertEquals(GameState.READY, gameStateTracker.getCurrentState());

        new GameStateChangeCommand(gameStateTracker, GameState.RUNNING).execute(null, CommandArguments.NONE);
        assertEquals(GameState.RUNNING, gameStateTracker.getCurrentState());

        new GameStateChangeCommand(gameStateTracker, GameState.FINISHED).execute(null, CommandArguments.NONE);
        assertEquals(GameState.FINISHED, gameStateTracker.getCurrentState());

        new GameStateChangeCommand(gameStateTracker, GameState.STANDBY).execute(null, CommandArguments.NONE);
        assertEquals(GameState.STANDBY, gameStateTracker.getCurrentState());
    }
}