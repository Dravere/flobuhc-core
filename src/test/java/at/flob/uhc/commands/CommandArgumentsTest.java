package at.flob.uhc.commands;


import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author Florian Graf
 */
public class CommandArgumentsTest
{
    @Test
    public void testEmptyArguments()
    {
        CommandArguments arguments = new CommandArguments(new String[0], 0);
        assertEquals(0, arguments.getLength());
    }

    @Test
    public void testOneArgument()
    {
        String[] input = { "one" };
        CommandArguments arguments = new CommandArguments(input, 0);

        assertEquals(1, arguments.getLength());
        assertEquals(input[0], arguments.getArgument(0));
    }

    @Test
    public void testTwoArguments()
    {
        String[] input = { "one", "two" };
        CommandArguments arguments = new CommandArguments(input, 0);

        assertEquals(2, arguments.getLength());
        assertEquals(input[0], arguments.getArgument(0));
        assertEquals(input[1], arguments.getArgument(1));
    }

    @Test
    public void testEmptyArgumentsWithOffset()
    {
        String[] input = { "one" };
        CommandArguments arguments = new CommandArguments(input, 1);

        assertEquals(0, arguments.getLength());
    }

    @Test
    public void testOneArgumentWithOffset()
    {
        String[] input = { "one", "two" };
        CommandArguments arguments = new CommandArguments(input, 1);

        assertEquals(1, arguments.getLength());
        assertEquals(input[1], arguments.getArgument(0));
    }

    @Test
    public void testTwoArgumentsWithOffset()
    {
        String[] input = { "one", "two", "three" };
        CommandArguments arguments = new CommandArguments(input, 1);

        assertEquals(2, arguments.getLength());
        assertEquals(input[1], arguments.getArgument(0));
        assertEquals(input[2], arguments.getArgument(1));
    }

    @Test
    public void testEmptyArgumentsWithOffsetAndReducedLength()
    {
        String[] input = { "one", "two" };
        CommandArguments arguments = new CommandArguments(input, 1, 0);

        assertEquals(0, arguments.getLength());
    }

    @Test
    public void testOneArgumentWithOffsetAndReducedLength()
    {
        String[] input = { "one", "two", "three" };
        CommandArguments arguments = new CommandArguments(input, 1, 1);

        assertEquals(1, arguments.getLength());
        assertEquals(input[1], arguments.getArgument(0));
    }

    @Test
    public void testTwoArgumentsWithOffsetAndReducedLength()
    {
        String[] input = { "one", "two", "three", "four" };
        CommandArguments arguments = new CommandArguments(input, 1, 2);

        assertEquals(2, arguments.getLength());
        assertEquals(input[1], arguments.getArgument(0));
        assertEquals(input[2], arguments.getArgument(1));
    }
}