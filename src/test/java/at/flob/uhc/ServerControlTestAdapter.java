// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.mockito.stubbing.Answer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


/**
 * @author Florian Graf
 */
public class ServerControlTestAdapter
    implements ServerControl
{
    public ServerControlTestAdapter()
    {
        m_isHealthRegenerationEnabled = true;
        m_listeners = new ArrayList<>();
        m_players = new ArrayList<>();
        m_defaultGameMode = GameMode.SURVIVAL;
        m_worlds = new HashMap<String, World>();
    }

    @Override
    public Collection<? extends Player> getOnlinePlayers()
    {
        return m_players;
    }

    @Override
    public void gatherPlayers(String worldName, double x, double y, double z)
    {
        if(Double.isNaN(y))
        {
            y = 128;
        }

        for(Player player : m_players)
        {
            player.teleport(new Location(null, x, y, z));
        }
    }

    public void addPlayer(Player player)
    {
        m_players.add(player);
    }

    @Override
    public void spreadPlayers(String worldName, double x, double z, double distance, double minimum, boolean keepTeams)
    {
    }

    @Override
    public void setHealthRegeneration(boolean enabled)
    {
        m_isHealthRegenerationEnabled = enabled;
    }

    public boolean isHealthRegenerationEnabled()
    {
        return m_isHealthRegenerationEnabled;
    }

    @Override
    public void setDefaultGameMode(GameMode gameMode)
    {
        m_defaultGameMode = gameMode;
    }

    public GameMode getDefaultGameMode()
    {
        return m_defaultGameMode;
    }

    @Override
    public void setPvp(boolean enabled)
    {
        m_pvp = enabled;
    }

    @Override
    public void setDifficulty(Difficulty difficulty)
    {
    }

    @Override
    public World getWorld(String name)
    {
        return m_worlds.get(name);
    }

    public void createWorld(String name)
    {
        WorldBorder border = mock(WorldBorder.class);
        doAnswer(invocation -> m_borderSize = invocation.getArgument(0)).when(border).setSize(any(Double.class));
        doAnswer(invocation -> {
            m_borderCenterX = invocation.getArgument(0);
            m_borderCenterZ = invocation.getArgument(1);
            return null;
        }).when(border).setCenter(any(Double.class), any(Double.class));

        World world = mock(World.class);
        when(world.getWorldBorder()).thenReturn(border);
        when(world.getName()).thenReturn(name);
        m_worlds.put(name, world);
    }

    @Override
    public Collection<World> getWorlds()
    {
        return m_worlds.values();
    }

    @Override
    public PermissionAttachment attachPermission(Permissible permissible)
    {
        return null;
    }

    public boolean isPvpEnabled()
    {
        return m_pvp;
    }

    @Override
    public void registerEventListener(Listener listener)
    {
        m_listeners.add(listener);
    }

    @Override
    public Scoreboard getMainScoreboard()
    {
        Scoreboard scoreboard = mock(Scoreboard.class);
        when(scoreboard.getTeams()).thenReturn(new HashSet<>());

        Objective objective = createObjective();
        when(scoreboard.registerNewObjective(any(String.class), any(String.class))).thenReturn(objective);
        return scoreboard;
    }

    @Override
    public PluginConfiguration getConfiguration()
    {
        return TestUtilities.getConfiguration();
    }

    @Override
    public PluginScheduler getScheduler()
    {
        PluginScheduler scheduler = mock(PluginScheduler.class);

        Answer<BukkitTask> answer = (args) -> {
            args.<Runnable>getArgument(0).run();
            return null;
        };

        when(scheduler.runTask(any(Runnable.class))).thenAnswer(answer);
        when(scheduler.runTaskDelayed(any(Runnable.class), any(Long.class))).thenAnswer(answer);

        return scheduler;
    }

    public void fireEvent(Object event)
    {
        Class<?> eventClass = event.getClass();
        for(Listener listener : m_listeners)
        {
            for(Method method : listener.getClass().getMethods())
            {
                if(method.getParameterCount() != 1)
                {
                    continue;
                }

                if(method.getParameterTypes()[0].equals(eventClass))
                {
                    try
                    {
                        method.setAccessible(true);
                        method.invoke(listener, event);
                        break;
                    }
                    catch(IllegalAccessException | InvocationTargetException exception)
                    {
                        throw new RuntimeException("Failed to invoke method on listener!", exception);
                    }
                }
            }
        }
    }

    public double getBorderCenterX()
    {
        return m_borderCenterX;
    }

    public double getBorderCenterZ()
    {
        return m_borderCenterZ;
    }

    public double getBorderSize()
    {
        return m_borderSize;
    }

    private Objective createObjective()
    {
        Objective objective = mock(Objective.class);
        Score score = mock(Score.class);

        when(objective.getScore(any(String.class))).thenReturn(score);
        when(objective.getScore((String)null)).thenReturn(score);
        return objective;
    }

    private boolean m_isHealthRegenerationEnabled;
    private Collection<Listener> m_listeners;
    private Collection<Player> m_players;
    private GameMode m_defaultGameMode;

    private HashMap<String, World> m_worlds;
    private double m_borderCenterX;
    private double m_borderCenterZ;
    private double m_borderSize;

    private boolean m_pvp;
}
