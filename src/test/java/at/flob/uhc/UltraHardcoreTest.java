package at.flob.uhc;


import at.flob.uhc.commands.CommandArguments;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.potion.PotionType;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * @author Florian Graf
 */
public class UltraHardcoreTest
{
//    @Test
//    public void testNormalPlayThrough()
//    {
//        Player one = TestUtilities.createPlayer();
//        Player two = TestUtilities.createPlayer();
//
//        ServerControlTestAdapter serverControl = new ServerControlTestAdapter();
//        serverControl.addPlayer(one);
//        serverControl.addPlayer(two);
//
//        UltraHardcore ultraHardcore = new UltraHardcore(serverControl);
//
//        BrewEvent brewEvent = TestUtilities.createBrewEvent(one, Material.GHAST_TEAR, PotionType.AWKWARD);
//        serverControl.fireEvent(brewEvent);
//        assertFalse(brewEvent.isCancelled());
//
//        CommandSender sender = mock(CommandSender.class);
//        when(sender.hasPermission(any(String.class))).thenReturn(true);
//
//        assertTrue(ultraHardcore.runCommand(sender, "create", CommandArguments.NONE));
//        assertEquals(GameMode.ADVENTURE, one.getGameMode());
//        assertEquals(GameMode.ADVENTURE, two.getGameMode());
//
//        assertEquals(0, serverControl.getBorderCenterX(), 0.0001);
//        assertEquals(0, serverControl.getBorderCenterZ(), 0.0001);
//        assertEquals(32, serverControl.getBorderSize(), 0.0001);
//
//        brewEvent = TestUtilities.createBrewEvent(one, Material.GHAST_TEAR, PotionType.AWKWARD);
//        serverControl.fireEvent(brewEvent);
//        assertTrue(brewEvent.isCancelled());
//
//        assertTrue(ultraHardcore.runCommand(sender, "exit", CommandArguments.NONE));
//        assertEquals(GameMode.SURVIVAL, one.getGameMode());
//        assertEquals(GameMode.SURVIVAL, two.getGameMode());
//
//        assertEquals(0, serverControl.getBorderCenterX(), 0.0001);
//        assertEquals(0, serverControl.getBorderCenterZ(), 0.0001);
//        assertEquals(30000000, serverControl.getBorderSize(), 0.0001);
//
//        brewEvent = TestUtilities.createBrewEvent(one, Material.GHAST_TEAR, PotionType.AWKWARD);
//        serverControl.fireEvent(brewEvent);
//        assertFalse(brewEvent.isCancelled());
//    }
}