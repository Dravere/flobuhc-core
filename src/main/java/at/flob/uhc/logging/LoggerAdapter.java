// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.logging;


import java.util.logging.LogRecord;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class LoggerAdapter
    extends Logger
{
    private static final String PREFIX = "[flobuhc-core] ";

    LoggerAdapter(String name)
    {
        super(name, null);
    }

    @Override
    public void log(LogRecord record)
    {
        record.setMessage(PREFIX + record.getMessage());
        super.log(record);
    }
}
