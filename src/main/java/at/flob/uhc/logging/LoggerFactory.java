// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.logging;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class LoggerFactory
{
    private static final LoggerFactory s_instance = new LoggerFactory();

    public static Logger getLogger(Class<?> owningClass)
    {
        if(owningClass == null)
        {
            throw new IllegalArgumentException("The owning class cannot be null!");
        }

        final String name = owningClass.getCanonicalName();

        if(name == null)
        {
            throw new IllegalArgumentException("The canonical name of the owning class is null!");
        }

        return s_instance.m_loggerMap.computeIfAbsent(name, k -> {
            Logger logger = new LoggerAdapter(name);

            if(s_instance.m_parent != null)
            {
                logger.setParent(s_instance.m_parent);
            }

            return logger;
        });
    }

    public static void setParent(final Logger parent)
    {
        if(parent == null)
        {
            throw new IllegalArgumentException("The parent cannot be null!");
        }

        s_instance.m_parent = parent;
        s_instance.m_loggerMap.forEach((k, v) -> v.setParent(parent));
    }

    private LoggerFactory()
    {
        m_loggerMap = new ConcurrentHashMap<>();
    }

    private Logger m_parent;
    private final Map<String, Logger> m_loggerMap;
}
