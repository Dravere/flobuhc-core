// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;


/**
 * Based on Spigot implementation of spread players.
 *
 * @author Florian Graf
 */
public class Spreader
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Spreader.class);

    private static final Random RANDOM = new Random();

    private static class Range
    {
        private double minimum;
        private double maximum;

        Range(double minimum, double maximum)
        {
            this.minimum = Math.min(minimum, maximum);
            this.maximum = Math.max(minimum, maximum);
        }

        double getMinimum()
        {
            return this.minimum;
        }

        double getMaximum()
        {
            return this.maximum;
        }

        double getRandomValue()
        {
            return RANDOM.nextDouble() * (this.maximum - this.minimum) + this.minimum;
        }
    }

    public static boolean spreadPlayers(
        World world,
        Collection<? extends Player> players,
        double centerX,
        double centerZ,
        double radius,
        double minDistance,
        boolean keepTeams)
    {
        if(world == null)
        {
            throw new IllegalArgumentException("The world cannot be null!");
        }

        if(minDistance < 0.0D)
        {
            throw new IllegalArgumentException("The minimum distance has to be zero or positive!");
        }

        if(radius < minDistance + 1.0D)
        {
            throw new IllegalArgumentException("The radius has to be greater than the minimum distance!");
        }

        final Range xRange = new Range(centerX - radius, centerX + radius);
        final Range zRange = new Range(centerZ - radius, centerZ + radius);

        final int spreadSize = keepTeams ? getTeams(players) : players.size();

        final Location[] locations = getInitialLocations(world, spreadSize, xRange, zRange);
        final boolean success = spreadLocations(world, minDistance, xRange, zRange, locations);

        if(!success)
        {
            LOGGER.warning(() -> String.format(
                "Could not spread %d %s around %.0f,%.0f with minimum distance %.2f (too many players for space)!",
                spreadSize,
                keepTeams ? "teams" : "players",
                centerX,
                centerZ,
                minDistance));

            return false;
        }

        final double distanceSpread = spreadPlayers(world, players, locations, keepTeams);

        LOGGER.info(() -> String.format(
            "Succesfully spread %d %s around %.0f,%.0f with minimum distance %.2f!",
            locations.length,
            keepTeams ? "teams" : "players",
            centerX,
            centerZ,
            minDistance));

        if(locations.length > 1)
        {
            LOGGER.info(() -> String.format(
                "Average distance between %s is %.2f blocks apart!",
                keepTeams ? "teams" : "players",
                distanceSpread));
        }

        return true;
    }

    private static boolean spreadLocations(
        World world,
        double minDistance,
        Range xRange,
        Range zRange,
        Location[] locations)
    {
        boolean hasInvalidLocations = true;

        // We compare squared distances
        minDistance *= minDistance;

        for(int i = 0; hasInvalidLocations && i < 10000; ++i)
        {
            hasInvalidLocations = false;

            for(Location location : locations)
            {
                int distanceTooCloseCount = 0;
                Location distanceVector = new Location(world, 0, 0, 0);

                for(Location otherLocation : locations)
                {
                    if(location != otherLocation)
                    {
                        double distance = location.distanceSquared(otherLocation);

                        if(distance < minDistance)
                        {
                            ++distanceTooCloseCount;

                            distanceVector.add(
                                otherLocation.getX() - location.getX(),
                                0,
                                otherLocation.getZ() - location.getZ());
                        }
                    }
                }

                if(distanceTooCloseCount > 0)
                {
                    distanceVector.setX(distanceVector.getX() / distanceTooCloseCount);
                    distanceVector.setZ(distanceVector.getZ() / distanceTooCloseCount);

                    double length = distanceVector.length();

                    if(length > 0.0D)
                    {
                        distanceVector.setX(distanceVector.getX() / length);
                        distanceVector.setZ(distanceVector.getZ() / length);

                        location.add(-distanceVector.getX(), 0, -distanceVector.getZ());
                    }
                    else
                    {
                        location.setX(xRange.getRandomValue());
                        location.setZ(zRange.getRandomValue());
                    }

                    hasInvalidLocations = true;
                }

                if(clampLocationXZ(location, xRange, zRange))
                {
                    hasInvalidLocations = true;
                }
            }

            if(!hasInvalidLocations)
            {
                for(Location location : locations)
                {
                    int y = Utilities.findSafeYForSpawn(world, location.getBlockX(), location.getBlockZ());

                    if(y < 0)
                    {
                        location.setX(xRange.getRandomValue());
                        location.setZ(zRange.getRandomValue());
                        hasInvalidLocations = true;
                    }
                    else
                    {
                        location.setY(y);
                    }
                }
            }
        }

        return !hasInvalidLocations;
    }

    private static boolean clampLocationXZ(Location location, Range xRange, Range zRange)
    {
        boolean wasClamped = false;

        if(location.getX() < xRange.getMinimum())
        {
            location.setX(xRange.getMinimum());
            wasClamped = true;
        }
        else if(location.getX() > xRange.getMaximum())
        {
            location.setX(xRange.getMaximum());
            wasClamped = true;
        }

        if(location.getZ() < zRange.getMinimum())
        {
            location.setZ(zRange.getMinimum());
            wasClamped = true;
        }
        else if(location.getZ() > zRange.getMaximum())
        {
            location.setZ(zRange.getMaximum());
            wasClamped = true;
        }

        return wasClamped;
    }

    private static double spreadPlayers(World world, Collection<? extends Player> list, Location[] locations, boolean teams)
    {
        Map<Team, Location> teamLocations = new HashMap<>();
        double distance = 0.0D;
        int i = 0;

        for(Player player : list)
        {
            Location location;

            if(teams)
            {
                Team team = player.getScoreboard().getEntryTeam(player.getName());

                location = teamLocations.get(team);
                if(location == null)
                {
                    location = locations[i++];
                    teamLocations.put(team, location);
                }
            }
            else
            {
                location = locations[i++];
            }

            player.teleport(new Location(
                world,
                location.getBlockX() + 0.5,
                location.getY(),
                location.getBlockZ() + 0.5));

            double value = Double.MAX_VALUE;

            for(Location location1 : locations)
            {
                if(location != location1)
                {
                    double d = location.distanceSquared(location1);
                    value = Math.min(d, value);
                }
            }

            distance += value;
        }

        distance /= list.size();
        return distance;
    }

    private static int getTeams(Collection<? extends Player> players)
    {
        Set<Team> teams = new HashSet<>();

        for(Player player : players)
        {
            teams.add(player.getScoreboard().getEntryTeam(player.getName()));
        }

        return teams.size();
    }

    private static Location[] getInitialLocations(
        World world,
        int size,
        Range xRange,
        Range zRange)
    {
        Location[] locations = new Location[size];

        for(int i = 0; i < size; ++i)
        {
            double x = xRange.getRandomValue();
            double z = zRange.getRandomValue();

            locations[i] = (new Location(world, x, 0, z));
        }

        return locations;
    }
}
