// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Collection;


/**
 * @author Florian Graf
 */
public interface ServerControl
{
    Collection<? extends Player> getOnlinePlayers();

    void gatherPlayers(String worldName, double x, double y, double z);

    void spreadPlayers(String worldName, double x, double z, double distance, double minimum, boolean keepTeams);

    void setHealthRegeneration(boolean enabled);

    void setDefaultGameMode(GameMode gameMode);

    void setPvp(boolean enabled);

    void setDifficulty(Difficulty difficulty);

    World getWorld(String name);

    Collection<World> getWorlds();

    PermissionAttachment attachPermission(Permissible permissible);

    void registerEventListener(Listener listener);

    Scoreboard getMainScoreboard();

    PluginConfiguration getConfiguration();

    PluginScheduler getScheduler();
}
