// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.commands.CommandArguments;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;


/**
 * The entry point of the plugin.
 *
 * @author Florian Graf
 */
public class FlobUhcCorePlugin
    extends JavaPlugin
    implements Listener
{
    @Override
    public void onEnable()
    {
        super.onEnable();

        UltraHardcore.setDataFolder(getDataFolder());

        saveDefaultConfig();

        LoggerFactory.setParent(getLogger());

        if(m_ultraHardcore == null)
        {
            ServerControl serverControl = new BukkitServerControl(this);
            m_ultraHardcore = new UltraHardcore(serverControl);
        }

        m_ultraHardcore.enable();
        getCommand("flobuhc").setExecutor(this);
    }

    @Override
    public void onDisable()
    {
        super.onDisable();

        m_ultraHardcore.disable();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if(args.length == 0 || args[0].equalsIgnoreCase("info"))
        {
            PluginDescriptionFile description = getDescription();
            sender.sendMessage(description.getFullName());
            sender.sendMessage(description.getWebsite());
            return true;
        }

        return m_ultraHardcore.runCommand(sender, args[0], new CommandArguments(args, 1));
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
    {
        List<String> result;

        if(args.length == 0)
        {
            result = m_ultraHardcore.getTabCompleteList(
                sender,
                null,
                new CommandArguments(args, 0, 0));

            result.add("info");
        }
        else
        {
            result = m_ultraHardcore.getTabCompleteList(sender, args[0], new CommandArguments(args, 1));

            if(args.length == 1 && "INFO".startsWith(args[0].toUpperCase()))
            {
                result.add("info");
            }
        }

        return result;
    }

    private UltraHardcore m_ultraHardcore;
}
