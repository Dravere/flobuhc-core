// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class BukkitServerControl
    implements ServerControl
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BukkitServerControl.class);

    public BukkitServerControl(JavaPlugin plugin)
    {
        m_plugin = plugin;
        m_configuration = new PluginConfiguration(plugin);
        m_scheduler = new PluginScheduler(plugin);
    }

    @Override
    public Collection<? extends Player> getOnlinePlayers()
    {
        return Bukkit.getOnlinePlayers();
    }

    @Override
    public void gatherPlayers(String worldName, double x, double y, double z)
    {
        World defaultWorld = findWorld(worldName);

        if(Double.isNaN(y))
        {
            y = defaultWorld.getHighestBlockYAt((int)x, (int)z);
        }

        for(Player player : Bukkit.getOnlinePlayers())
        {
            player.teleport(new Location(defaultWorld, x, y, z));
        }
    }

    @Override
    public void spreadPlayers(String worldName, double x, double z, double radius, double minimum, boolean keepTeams)
    {
        World world = findWorld(worldName);

        if(!Spreader.spreadPlayers(world, Bukkit.getOnlinePlayers(), x, z, radius, minimum, keepTeams))
        {
            LOGGER.warning(() -> "Spreading players failed!");
        }
    }

    @Override
    public void setHealthRegeneration(boolean enabled)
    {
        for(World world : Bukkit.getWorlds())
        {
            Utilities.setHealthRegeneration(world, enabled);
        }
    }

    @Override
    public void setDefaultGameMode(GameMode gameMode)
    {
        Bukkit.setDefaultGameMode(gameMode);
    }

    @Override
    public void setPvp(boolean enabled)
    {
        for(World world : Bukkit.getWorlds())
        {
            world.setPVP(enabled);
        }
    }

    @Override
    public void setDifficulty(Difficulty difficulty)
    {
        for(World world : Bukkit.getWorlds())
        {
            world.setDifficulty(difficulty);
        }
    }

    @Override
    public World getWorld(String name)
    {
        return Bukkit.getWorld(name);
    }

    @Override
    public Collection<World> getWorlds()
    {
        return Bukkit.getWorlds();
    }

    @Override
    public PermissionAttachment attachPermission(Permissible permissible)
    {
        return permissible.addAttachment(m_plugin);
    }

    @Override
    public void registerEventListener(Listener listener)
    {
        Bukkit.getPluginManager().registerEvents(listener, m_plugin);
    }

    @Override
    public Scoreboard getMainScoreboard()
    {
        return Bukkit.getScoreboardManager().getMainScoreboard();
    }

    @Override
    public PluginConfiguration getConfiguration()
    {
        return m_configuration;
    }

    @Override
    public PluginScheduler getScheduler()
    {
        return m_scheduler;
    }

    private World findWorld(String worldName)
    {
        World defaultWorld = Bukkit.getWorld(worldName);

        if(defaultWorld == null)
        {
            LOGGER.warning(() -> "World " + worldName + " not found, searching for an Overworld!");

            List<World> worlds = Bukkit.getWorlds();

            for(World world : worlds)
            {
                if(world.getEnvironment() == World.Environment.NORMAL)
                {
                    defaultWorld = world;
                }
            }

            if(defaultWorld == null)
            {
                LOGGER.warning(() -> "No Overworld found! Taking first available world!");
                defaultWorld = worlds.get(0);
            }
        }
        return defaultWorld;
    }

    private final JavaPlugin m_plugin;
    private final PluginConfiguration m_configuration;
    private final PluginScheduler m_scheduler;
}
