// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class TeamManager
{
    private static final Logger s_logger = LoggerFactory.getLogger(TeamManager.class);

    public TeamManager(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_scoreboard = serverControl.getMainScoreboard();
        m_serverControl = serverControl;

        m_random = new Random();
        m_availableTeamColors = new ArrayList<>(16);

        initialiseTeamColors();
    }

    public void clearTeams()
    {
        for(Team team : m_scoreboard.getTeams())
        {
            team.unregister();
        }

        initialiseTeamColors();
    }

    public void joinTeam(Player player, String teamName)
    {
        Team team = m_scoreboard.getTeam(teamName);
        if(team == null)
        {
            team = m_scoreboard.registerNewTeam(teamName);
            team.setDisplayName(teamName);

            if(m_availableTeamColors.size() > 0)
            {
                int index = m_random.nextInt(m_availableTeamColors.size());
                changeTeamColor(team, index);
            }
        }

        Team previousTeam = m_scoreboard.getEntryTeam(player.getName());

        team.addEntry(player.getName());

        if(previousTeam != null)
        {
            player.sendMessage(
                "You left team: " + team.getColor() + team.getName() + ChatColor.RESET);
        }

        for(Player onlinePlayer : m_serverControl.getOnlinePlayers())
        {
            onlinePlayer.sendMessage(
                player.getName() + " joined team " + team.getColor() + team.getName() + ChatColor.RESET);
        }

        if(previousTeam != null && previousTeam.getSize() == 0)
        {
            ChatColor teamColor = previousTeam.getColor();

            if(teamColor != null)
            {
                m_availableTeamColors.add(teamColor);
            }

            previousTeam.unregister();
        }
    }

    public Team getTeam(Player player)
    {
        return m_scoreboard.getEntryTeam(player.getName());
    }

    public Set<String> getTeamNames()
    {
        Set<String> teamNames = new HashSet<>();

        for(Team team : m_scoreboard.getTeams())
        {
            teamNames.add(team.getName());
        }

        return teamNames;
    }

    public boolean updateTeamColor(String teamName, ChatColor color)
    {
        Team teamToChangeColor = null;
        for(Team team : m_scoreboard.getTeams())
        {
            if(team.getName().equals(teamName))
            {
                teamToChangeColor = team;
                break;
            }
        }

        if(teamToChangeColor != null)
        {
            for(int i = 0; i < m_availableTeamColors.size(); ++i)
            {
                if(m_availableTeamColors.get(i) == color)
                {
                    changeTeamColor(teamToChangeColor, i);
                    return true;
                }
            }

            s_logger.warning(() -> "Shouldn't happen here. Unavailable color: " + color);
        }
        else
        {
            s_logger.warning(() -> "Shouldn't happen here. Unknown team name: " + teamName);
        }

        return false;
    }

    public ChatColor[] getAvailableColors()
    {
        return m_availableTeamColors.toArray(new ChatColor[m_availableTeamColors.size()]);
    }

    private void changeTeamColor(Team team, int colorIndex)
    {
        ChatColor currentTeamColor = team.getColor();
        if(currentTeamColor != null)
        {
            m_availableTeamColors.add(currentTeamColor);
        }

        ChatColor teamColor = m_availableTeamColors.remove(colorIndex);
        team.setColor(teamColor);

        String prefix = m_serverControl.getConfiguration().getString(
            PluginConfiguration.GAME_TEAMS_PREFIX,
            "{teamColor}");

        String suffix = m_serverControl.getConfiguration().getString(
            PluginConfiguration.GAME_TEAMS_SUFFIX,
            ChatColor.RESET.toString());

        prefix = replaceTeamPlaceholders(prefix, team);
        suffix = replaceTeamPlaceholders(suffix, team);

        team.setPrefix(prefix);
        team.setSuffix(suffix);
    }

    private String replaceTeamPlaceholders(String text, Team team)
    {
        return text.replace("{teamColor}", team.getColor().toString())
            .replace("{teamName}", team.getName());
    }

    private void initialiseTeamColors()
    {
        m_availableTeamColors.clear();
        m_availableTeamColors.add(ChatColor.AQUA);
        m_availableTeamColors.add(ChatColor.BLACK);
        m_availableTeamColors.add(ChatColor.BLUE);
        m_availableTeamColors.add(ChatColor.DARK_AQUA);
        m_availableTeamColors.add(ChatColor.DARK_BLUE);
        m_availableTeamColors.add(ChatColor.DARK_GRAY);
        m_availableTeamColors.add(ChatColor.DARK_GREEN);
        m_availableTeamColors.add(ChatColor.DARK_PURPLE);
        m_availableTeamColors.add(ChatColor.DARK_RED);
        m_availableTeamColors.add(ChatColor.GOLD);
        m_availableTeamColors.add(ChatColor.GRAY);
        m_availableTeamColors.add(ChatColor.GREEN);
        m_availableTeamColors.add(ChatColor.LIGHT_PURPLE);
        m_availableTeamColors.add(ChatColor.RED);
        m_availableTeamColors.add(ChatColor.YELLOW);
    }

    private final Scoreboard m_scoreboard;
    private final ServerControl m_serverControl;

    private final Random m_random;
    private final List<ChatColor> m_availableTeamColors;
}
