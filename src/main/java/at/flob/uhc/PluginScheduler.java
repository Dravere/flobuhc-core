// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.Timer;
import java.util.TimerTask;


/**
 * @author Florian Graf
 */
public class PluginScheduler
{
    public static final long TICKS_PER_SECOND = 20;

    public static long ms2t(long milliseconds)
    {
        return s2t(milliseconds) / 1000;
    }

    public static long s2t(long seconds)
    {
        return seconds * TICKS_PER_SECOND;
    }

    public PluginScheduler(Plugin plugin)
    {
        if(plugin == null)
        {
            throw new IllegalArgumentException("The plugin cannot be null!");
        }

        m_timer = new Timer(PluginScheduler.class.getName(),true);
        m_plugin = plugin;
    }

    public BukkitTask runTask(Runnable runnable)
    {
        return m_plugin.getServer().getScheduler().runTask(m_plugin, runnable);
    }

    public BukkitTask runTaskDelayed(Runnable runnable, long delayTicks)
    {
        return m_plugin.getServer().getScheduler().runTaskLater(m_plugin, runnable, delayTicks);
    }

    public TimerTask scheduleDelayed(Runnable runnable, long delayMilliseconds)
    {
        TimerTask timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                runTask(runnable);
            }
        };

        m_timer.schedule(timerTask, delayMilliseconds);
        return timerTask;
    }

    public TimerTask scheduleRepeated(Runnable runnable, long delayMilliseconds, long periodMilliseconds)
    {
        TimerTask timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                runTask(runnable);
            }
        };

        m_timer.schedule(timerTask, delayMilliseconds, periodMilliseconds);
        return timerTask;
    }

    private final Timer m_timer;
    private final Plugin m_plugin;
}
