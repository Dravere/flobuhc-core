// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.Melody;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Locale;
import java.util.TimerTask;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class EpisodeTimerConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(EpisodeTimerConfiguration.class);

    private static final long DEFAULT_EPISODE_DURATION = 20;

    public EpisodeTimerConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(m_runningTimer != null)
        {
            m_runningTimer.cancel();
            m_runningTimer = null;
        }

        m_currentEpisode = 1;

        if(newState == GameState.RUNNING)
        {
            long duration = getEpisodeDuration();

            PluginScheduler scheduler = m_serverControl.getScheduler();
            m_runningTimer = scheduler.scheduleRepeated(this::announceEpisodeEnding, duration, duration);

            s_logger.info(() -> "Started episode timer for every " + (duration / 1000 / 60) + " minutes!");
        }
    }

    private long getEpisodeDuration()
    {
        long durationInMinutes = m_serverControl.getConfiguration().getLong(
            PluginConfiguration.GAME_TIMER_EPISODE,
            DEFAULT_EPISODE_DURATION);

        return durationInMinutes * 60 * 1000;
    }

    private void announceEpisodeEnding()
    {
        String message = ChatColor.GOLD + String.format(Locale.ENGLISH, "END OF EPISODE %d!", m_currentEpisode);

        Collection<? extends Player> players = m_serverControl.getOnlinePlayers();
        for(Player player : players)
        {
            player.sendMessage(message);
        }

        s_logger.info(() -> "Episode message sent: " + message);

        Melody.EPISODE_ENDING.play(m_serverControl.getScheduler(), players, Instrument.PIANO);

        ++m_currentEpisode;
    }

    private final ServerControl m_serverControl;

    private int m_currentEpisode;
    private TimerTask m_runningTimer;
}
