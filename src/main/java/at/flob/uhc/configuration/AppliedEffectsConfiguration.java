// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.Utilities;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class AppliedEffectsConfiguration
    implements GameStateListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AppliedEffectsConfiguration.class);

    private static final PotionEffect BLINDNESS = new PotionEffect(
        PotionEffectType.BLINDNESS,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect SLOWNESS = new PotionEffect(
        PotionEffectType.SLOW,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect HEALING = new PotionEffect(
        PotionEffectType.HEAL,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect SATURATION = new PotionEffect(
        PotionEffectType.SATURATION,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect WATER_BREATHING = new PotionEffect(
        PotionEffectType.WATER_BREATHING,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect DAMAGE_RESISTANCE = new PotionEffect(
        PotionEffectType.DAMAGE_RESISTANCE,
        1000000,
        255,
        false,
        false);

    private static final PotionEffect FIRE_RESISTANCE = new PotionEffect(
        PotionEffectType.FIRE_RESISTANCE,
        1000000,
        255,
        false,
        false);

    public AppliedEffectsConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
        m_appliedPotionEffects = new ArrayList<>();

        m_serverControl.registerEventListener(new Listener()
        {
            @EventHandler
            public void onPlayerJoin(PlayerJoinEvent event)
            {
                AppliedEffectsConfiguration.this.onPlayerJoin(event);
            }

            @EventHandler
            public void onPlayerRespawn(PlayerRespawnEvent event)
            {
                AppliedEffectsConfiguration.this.onPlayerRespawn(event);
            }
        });
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        clearPotionEffects();

        if(newState == GameState.CREATED)
        {
            m_appliedPotionEffects.add(HEALING);
            m_appliedPotionEffects.add(SATURATION);
            m_appliedPotionEffects.add(WATER_BREATHING);
            m_appliedPotionEffects.add(FIRE_RESISTANCE);
            m_appliedPotionEffects.add(DAMAGE_RESISTANCE);
        }
        else if(newState == GameState.READY)
        {
            m_appliedPotionEffects.add(BLINDNESS);
            m_appliedPotionEffects.add(SLOWNESS);
            m_appliedPotionEffects.add(HEALING);
            m_appliedPotionEffects.add(SATURATION);
            m_appliedPotionEffects.add(WATER_BREATHING);
            m_appliedPotionEffects.add(FIRE_RESISTANCE);
            m_appliedPotionEffects.add(DAMAGE_RESISTANCE);
        }
        else if(newState == GameState.RUNNING)
        {
            int protectionTime = m_serverControl.getConfiguration().getInt(
                PluginConfiguration.GAME_COMBAT_PROTECTION,
                0);

            if(protectionTime > 0)
            {
                for(Player player : m_serverControl.getOnlinePlayers())
                {
                    if(GameModeConfiguration.isAlive(player))
                    {
                        player.addPotionEffect(new PotionEffect(
                            PotionEffectType.DAMAGE_RESISTANCE,
                            (int)PluginScheduler.s2t(protectionTime * 60),
                            255,
                            false,
                            false));
                    }
                }
            }
        }

        applyPotionEffect();
    }

    private void applyPotionEffect()
    {
        LOGGER.info(() -> "Applying " + m_appliedPotionEffects.size() + " potion effects to online players!");

        if(m_appliedPotionEffects.isEmpty())
        {
            return;
        }

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            applyPotionEffects(player);
        }
    }

    public void applyPotionEffects(Player player)
    {
        for(PotionEffect potionEffect : m_appliedPotionEffects)
        {
            player.addPotionEffect(potionEffect, true);
        }
    }

    private void clearPotionEffects()
    {
        LOGGER.info(() -> "Clearing all potion effects from online players!");
        m_appliedPotionEffects.clear();

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            Utilities.clearPotionEffects(player);
        }
    }

    private void onPlayerJoin(PlayerJoinEvent event)
    {
        if(!TrainingConfiguration.isTrainee(event.getPlayer()))
        {
            LOGGER.info(() -> "Applying configured potions to player " + event.getPlayer().getName() + " on join!");
            applyPotionEffects(event.getPlayer());
        }
    }

    private void onPlayerRespawn(PlayerRespawnEvent event)
    {
        if(!TrainingConfiguration.isTrainee(event.getPlayer()))
        {
            LOGGER.info(() -> "Applying configured potions to player " + event.getPlayer().getName() + " on respawn!");
            m_serverControl.getScheduler().runTaskDelayed(() -> applyPotionEffects(event.getPlayer()), 5);
        }
    }

    private final ServerControl m_serverControl;
    private final List<PotionEffect> m_appliedPotionEffects;
}
