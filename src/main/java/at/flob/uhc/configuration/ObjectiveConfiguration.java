// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.ServerControl;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;


/**
 * @author Florian Graf
 */
public class ObjectiveConfiguration
    implements GameStateListener
{
    private final String HEALTH_OBJECTIVE_NAME = "flob-uhc-health";

    public ObjectiveConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        Scoreboard scoreboard = m_serverControl.getMainScoreboard();
        Objective objective = scoreboard.getObjective(HEALTH_OBJECTIVE_NAME);

        if(previousState == GameState.STANDBY && newState == GameState.CREATED)
        {
            if(objective == null)
            {
                objective = scoreboard.registerNewObjective(HEALTH_OBJECTIVE_NAME, "health");
            }

            objective.setDisplaySlot(DisplaySlot.PLAYER_LIST);

            updateHealthObjectiveScore(objective);
        }
        else if(newState == GameState.READY)
        {
            if(objective != null)
            {
                updateHealthObjectiveScore(objective);
            }
        }
        else if(newState == GameState.STANDBY)
        {
            if(objective != null)
            {
                objective.unregister();
            }
        }
    }

    private void updateHealthObjectiveScore(Objective objective)
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            Score score = objective.getScore(player.getName());
            score.setScore((int)Math.round(player.getHealth()));
        }
    }

    private final ServerControl m_serverControl;
}
