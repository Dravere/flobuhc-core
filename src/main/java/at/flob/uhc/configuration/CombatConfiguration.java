// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.Melody;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class CombatConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(CombatConfiguration.class);

    public CombatConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(m_bukkitTask != null)
        {
            m_bukkitTask.cancel();
            m_bukkitTask = null;
        }

        if(newState == GameState.RUNNING)
        {
            long holdOff = m_serverControl.getConfiguration().getLong(PluginConfiguration.GAME_COMBAT_HOLD_OFF, 0);

            if(holdOff > 0)
            {
                s_logger.info(() -> "Disabling PVP and holding it off for " + holdOff + " minutes!");

                makeAnnouncement(ChatColor.GOLD + "PVP will be enabled in " + holdOff + " minutes!");

                m_bukkitTask = m_serverControl.getScheduler()
                    .runTaskDelayed(this::enablePvp, PluginScheduler.s2t(holdOff * 60));
            }
            else
            {
                enablePvp();
            }
        }
        else if(newState == GameState.STANDBY)
        {
            s_logger.info(() -> "Enabling PVP");
            m_serverControl.setPvp(true);
        }
        else
        {
            s_logger.info(() -> "Disable PVP");
            m_serverControl.setPvp(false);
        }
    }

    private void enablePvp()
    {
        s_logger.info(() -> "Enabling PVP");
        m_serverControl.setPvp(true);

        makeAnnouncement(ChatColor.GOLD + "PVP HAS BEEN ENABLED!");

        Melody.PVP_ENABLED.play(m_serverControl.getScheduler(), m_serverControl.getOnlinePlayers(), Instrument.PIANO);
    }

    private void makeAnnouncement(String message)
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            player.sendMessage(message);
        }
    }

    private BukkitTask m_bukkitTask;

    private final ServerControl m_serverControl;
}
