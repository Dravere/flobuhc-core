// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class CageConfiguration
    implements GameStateListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CageConfiguration.class);

    public CageConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(m_checkPlayerLocationTask != null)
        {
            m_checkPlayerLocationTask.cancel();
            m_checkPlayerLocationTask = null;
        }

        switch(newState)
        {
        case CREATED:
            m_cagePlayers = true;
            startCheckPlayerPositions(
                PluginConfiguration.GAME_AREA_START_CAGEY,
                PluginConfiguration.GAME_AREA_START_CENTER_X,
                PluginConfiguration.GAME_AREA_START_CENTER_Y,
                PluginConfiguration.GAME_AREA_START_CENTER_Z,
                PluginConfiguration.GAME_AREA_START_WORLD);
            break;
        case READY:
        case RUNNING:
        case FINISHED:
            m_cagePlayers = true;
            startCheckPlayerPositions(
                PluginConfiguration.GAME_AREA_PLAY_CAGEY,
                PluginConfiguration.GAME_AREA_PLAY_CENTER_X,
                PluginConfiguration.GAME_AREA_PLAY_CENTER_Y,
                PluginConfiguration.GAME_AREA_PLAY_CENTER_Z,
                PluginConfiguration.GAME_AREA_PLAY_WORLD);
            break;
        default:
            m_cagePlayers = false;
            break;
        }
    }

    private void startCheckPlayerPositions(
        String pathCageY,
        String pathCenterX,
        String pathCenterY,
        String pathCenterZ,
        String pathWorldName)
    {
        PluginConfiguration config = m_serverControl.getConfiguration();
        double cageY = config.getDouble(pathCageY, Double.NaN);

        if(Double.isNaN(cageY))
        {
            return;
        }

        String worldName = config.getString(pathWorldName, "world");
        World world = m_serverControl.getWorld(worldName);

        if(world == null)
        {
            LOGGER.warning(() -> "Unable to enforce y cage because the world '" + worldName + "' wasn't found!");
            return;
        }

        double centerX = config.getDouble(pathCenterX, 0);
        double centerY = config.getDouble(pathCenterY, Double.NaN);
        double centerZ = config.getDouble(pathCenterZ, 0);

        if(Double.isNaN(centerY))
        {
            centerY = world.getHighestBlockYAt((int)centerX, (int)centerZ);
        }

        checkPlayerPositions(world, cageY, centerX, centerY, centerZ);
    }

    private void scheduleNextCheck(World world, double cageY, double centerX, double centerY, double centerZ)
    {
        m_checkPlayerLocationTask = m_serverControl.getScheduler()
            .runTaskDelayed(
                () -> checkPlayerPositions(world, cageY, centerX, centerY, centerZ),
                PluginScheduler.ms2t(500));
    }

    private void checkPlayerPositions(World world, double cageY, double centerX, double centerY, double centerZ)
    {
        if(!m_cagePlayers)
        {
            return;
        }

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(player.getWorld() == world && player.getLocation().getY() < cageY && !player.isDead())
            {
                double y = centerY;
                if(Double.isNaN(y))
                {
                    y = player.getWorld().getHighestBlockYAt((int)centerX, (int)centerZ);
                }

                player.setVelocity(new Vector(0, 0, 0));
                player.setFallDistance(0);
                player.teleport(new Location(player.getWorld(), centerX, y, centerZ));
            }
        }

        scheduleNextCheck(world, cageY, centerX, centerY, centerZ);
    }

    private final ServerControl m_serverControl;

    private boolean m_cagePlayers;
    private BukkitTask m_checkPlayerLocationTask;
}
