// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import org.bukkit.World;


/**
 * @author Florian Graf
 */
public class DaylightCycleConfiguration
    implements GameStateListener
{
    public DaylightCycleConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        switch(newState)
        {
        case CREATED:
            applyConfiguration(
                PluginConfiguration.GAME_DAYLIGHT_START_ENABLED,
                PluginConfiguration.GAME_DAYLIGHT_START_TIME);
            break;
        case READY:
            applyConfiguration(
                null, // Will default to false
                PluginConfiguration.GAME_DAYLIGHT_PLAY_TIME);
            break;
        case RUNNING:
            applyConfiguration(
                PluginConfiguration.GAME_DAYLIGHT_PLAY_ENABLED,
                PluginConfiguration.GAME_DAYLIGHT_PLAY_TIME);
            break;
        case FINISHED:
            applyConfiguration(
                PluginConfiguration.GAME_DAYLIGHT_FINISH_ENABLED,
                PluginConfiguration.GAME_DAYLIGHT_FINISH_TIME);
            break;
        default:
        case STANDBY:
            applyConfiguration(false, 6000);
            break;
        }
    }

    private void applyConfiguration(String doDaylightCycleConfigPath, String timeConfigPath)
    {
        PluginConfiguration config = m_serverControl.getConfiguration();

        boolean doDayLightCycle = false;
        if(doDaylightCycleConfigPath != null)
        {
            doDayLightCycle = config.getBoolean(doDaylightCycleConfigPath, true);
        }

        long time = config.getLong(timeConfigPath, 1000L);

        applyConfiguration(doDayLightCycle, time);
    }

    private void applyConfiguration(boolean doDaylightCycle, long time)
    {
        for(World world : m_serverControl.getWorlds())
        {
            world.setGameRuleValue("doDaylightCycle", doDaylightCycle ? "true" : "false");
            world.setTime(time);
        }
    }

    private final ServerControl m_serverControl;
}
