// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import at.flob.uhc.commands.NightVisionCommand;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class GameModeConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(GameModeConfiguration.class);

    private static final Map<UUID, PlayerMode> s_playerModes = new HashMap<>();

    /**
     * Toggles if the player is spectating.<br>
     * <br>
     * <b>Note: This does only changes his tracked state but does change his game mode or anything like that.</b>
     *
     * @param player The player for which the state should be changed.
     * @return If the player is now a spectator true; otherwise false.
     */
    public static PlayerMode toggleSpectator(Player player)
    {
        PlayerMode mode = s_playerModes.get(player.getUniqueId());
        if(mode == null || mode == PlayerMode.COMBATANT)
        {
            mode = PlayerMode.SPECTATOR;
        }
        else
        {
            mode = PlayerMode.COMBATANT;
        }

        s_playerModes.put(player.getUniqueId(), mode);
        return mode;
    }

    public static void markSpectating(Player player)
    {
        s_playerModes.put(player.getUniqueId(), PlayerMode.SPECTATOR);
    }

    public static void cancelSpectating(Player player)
    {
        markPlayerAlive(player);
    }

    public static void markPlayerAlive(Player player)
    {
        s_playerModes.put(player.getUniqueId(), PlayerMode.COMBATANT);
    }

    public static void markPlayerDead(Player player)
    {
        markSpectating(player);
    }

    public static boolean isAlive(Player player)
    {
        return s_playerModes.get(player.getUniqueId()) == PlayerMode.COMBATANT;
    }

    public static boolean isSpectating(Player player)
    {
        return s_playerModes.get(player.getUniqueId()) == PlayerMode.SPECTATOR;
    }

    GameModeConfiguration(ServerControl serverControl)
    {
        m_serverControl = serverControl;
        m_serverControl.registerEventListener(new Listener()
        {
            @EventHandler
            public void onPlayerJoin(PlayerJoinEvent event)
            {
                GameModeConfiguration.this.onPlayerJoin(event);
            }

            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent event)
            {
                GameModeConfiguration.this.onPlayerQuit(event);
            }

            @EventHandler
            public void onPlayerRespawn(PlayerRespawnEvent event)
            {
                GameModeConfiguration.this.onPlayerRespawn(event);
            }

            @EventHandler
            public void onPlayerDeath(PlayerDeathEvent event)
            {
                GameModeConfiguration.this.onPlayerDeath(event);
            }

            @EventHandler
            public void onPlayerInteractEntity(PlayerInteractEntityEvent event)
            {
                GameModeConfiguration.this.onPlayerInteractEntity(event);
            }
        });
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(newState == GameState.CREATED)
        {
            m_gameIsRunning = false;

            updateMultiverseCompatibility();
            updateCurrentGameMode(GameMode.ADVENTURE);
        }
        else if(newState == GameState.STANDBY)
        {
            m_gameIsRunning = false;

            updateCurrentGameMode(GameMode.SURVIVAL);
        }
        else if(newState == GameState.READY)
        {
            m_gameIsRunning = true;

            markPlayersAlive();
            updateCurrentGameMode(GameMode.ADVENTURE);
        }
        else if(newState == GameState.RUNNING)
        {
            m_gameIsRunning = true;

            updateCurrentGameMode(GameMode.SURVIVAL);
        }
        else if(newState == GameState.FINISHED)
        {
            m_gameIsRunning = true;

            markAllPlayersDead();
            updateCurrentGameMode(GameMode.SPECTATOR);
        }
    }

    private void onPlayerJoin(PlayerJoinEvent event)
    {
        if(m_currentGameMode == null)
        {
            return;
        }

        Player player = event.getPlayer();
        updateMultiverseCompatibility(player);

        GameMode updateGameMode;

        if(m_gameIsRunning && !isAlive(player))
        {
            updateGameMode = GameMode.SPECTATOR;
            event.setJoinMessage(null);

            markSpectating(player);
            displaySpectatorHelp(player);

            NightVisionCommand.giveNightVision(player);
        }
        else
        {
            updateGameMode = m_currentGameMode;
        }

        player.setGameMode(updateGameMode);

        s_logger.info(() -> "Joining player " + player.getName() + " set game mode to " + updateGameMode);
    }

    private void onPlayerQuit(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();

        if(m_gameIsRunning && !isAlive(player))
        {
            event.setQuitMessage(null);
            s_logger.info(() -> "Player " + player.getName() + " is quitting! Hiding message as it is a spectator in " +
                "a running game!");
        }
    }

    private void onPlayerRespawn(PlayerRespawnEvent event)
    {
        if(m_currentGameMode == null)
        {
            return;
        }

        GameMode updateGameMode;
        Player player = event.getPlayer();

        if(m_gameIsRunning && !isAlive(player))
        {
            updateGameMode = GameMode.SPECTATOR;
            displaySpectatorHelp(player);

            // Seem we have to delay this to the next tick as otherwise the game mode isn't updated properly
            m_serverControl.getScheduler().runTaskDelayed(() -> {
                player.setGameMode(updateGameMode);
                player.setCanPickupItems(true);
                player.setCollidable(true);
                NightVisionCommand.giveNightVision(player);
            }, 1L);

            // Temporary set the player to not pick up item and collide with anything (spectator emulation)
            player.setCollidable(false);
            player.setCanPickupItems(false);
        }
        else
        {
            updateGameMode = m_currentGameMode;

            // Seem we have to delay this to the next tick as otherwise the game mode isn't updated properly
            m_serverControl.getScheduler().runTaskDelayed(() -> player.setGameMode(updateGameMode), 0L);
        }

        s_logger.info(() -> "Re-spawning player " + player.getName() + " set game mode to " + updateGameMode);
    }

    private void onPlayerDeath(PlayerDeathEvent event)
    {
        if(!m_gameIsRunning)
        {
            return;
        }

        Player player = event.getEntity();
        player.setBedSpawnLocation(player.getLocation(), true);

        markPlayerDead(player);
    }

    private void onPlayerInteractEntity(PlayerInteractEntityEvent event)
    {
        Player player = event.getPlayer();

        if(player.getGameMode() != GameMode.SPECTATOR || !isSpectating(player))
        {
            return;
        }

        Entity entity = event.getRightClicked();
        if(!(entity instanceof Player))
        {
            return;
        }

        if(!m_serverControl.getConfiguration()
            .getBoolean(PluginConfiguration.GAME_SPECTATING_VIEW_PLAYER_INVENTORY, true))
        {
            return;
        }

        Player rightClickedPlayer = (Player)entity;

        Inventory displayInventory = Bukkit.createInventory(rightClickedPlayer,
            5 * 9,
            rightClickedPlayer.getDisplayName());

        PlayerInventory playerInventory = rightClickedPlayer.getInventory();

        displayInventory.setItem(0, playerInventory.getHelmet());
        displayInventory.setItem(1, playerInventory.getChestplate());
        displayInventory.setItem(2, playerInventory.getLeggings());
        displayInventory.setItem(3, playerInventory.getBoots());
        displayInventory.setItem(8, playerInventory.getItemInOffHand());

        ItemStack[] itemStacks = playerInventory.getStorageContents();
        for(int i = 9; i < itemStacks.length; ++i)
        {
            displayInventory.setItem(i, itemStacks[i]);
        }

        for(int i = 0; i < 9; ++i)
        {
            displayInventory.setItem(i + 4 * 9, itemStacks[i]);
        }

        player.openInventory(displayInventory);
    }

    private void updateCurrentGameMode(GameMode gameMode)
    {
        s_logger.info(() -> "Changing current game mode for players to " + gameMode);
        m_currentGameMode = gameMode;

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(m_gameIsRunning && isSpectating(player))
            {
                player.setGameMode(GameMode.SPECTATOR);
                displaySpectatorHelp(player);

                NightVisionCommand.giveNightVision(player);
            }
            else
            {
                player.setGameMode(m_currentGameMode);
            }
        }

        m_serverControl.setDefaultGameMode(m_currentGameMode);
    }

    private void markPlayersAlive()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(!isSpectating(player))
            {
                markPlayerAlive(player);
            }
        }
    }

    private void markAllPlayersDead()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            markPlayerDead(player);
        }
    }

    private void updateMultiverseCompatibility()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            updateMultiverseCompatibility(player);
        }
    }

    private void updateMultiverseCompatibility(Player player)
    {
        if(m_serverControl.getConfiguration().getBoolean(PluginConfiguration.COMPATIBILITY_MULTIVERSE, false))
        {
            PermissionAttachment permissions = m_serverControl.attachPermission(player);

            for(World world : m_serverControl.getWorlds())
            {
                String permission = "mv.bypass.gamemode." + world.getName();

                if(!player.hasPermission(permission))
                {
                    s_logger.info(() -> "For MultiVerse compatibility giving permission " + permission + " to " + player
                        .getName());

                    permissions.setPermission(permission, true);
                }
            }
        }
    }

    private void displaySpectatorHelp(Player player)
    {
        player.sendMessage(ChatColor.GOLD + "You are now spectating this game!");
        player.sendMessage(ChatColor.GOLD + "- Press your middle mouse button (e.g. mouse wheel) to select a player " +
            "to teleport to");
        player.sendMessage(ChatColor.GOLD + "- Right click other players to see their inventories");
        player.sendMessage(ChatColor.GOLD + "- Right click chest, furnaces and other items with storage" +
            " to see their inventories");
        player.sendMessage(ChatColor.GOLD + "- Right click mobs to see the world from their perspective, " +
            "sneak to get out");
        player.sendMessage(ChatColor.GOLD + "- Toggle night vision with the command: /flobuhc nightVision");
    }

    private final ServerControl m_serverControl;

    private boolean m_gameIsRunning;
    private GameMode m_currentGameMode;
}
