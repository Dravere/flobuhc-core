// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.ServerControl;
import org.bukkit.Difficulty;


/**
 * @author Florian Graf
 */
public class DifficultyConfiguration
    implements GameStateListener
{
    public DifficultyConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        m_serverControl.setDifficulty(Difficulty.HARD);
    }

    private final ServerControl m_serverControl;
}
