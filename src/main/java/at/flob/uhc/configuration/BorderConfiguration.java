// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.Melody;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.Locale;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class BorderConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(BorderConfiguration.class);

    BorderConfiguration(ServerControl serverControl)
    {
        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(m_bukkitTask != null)
        {
            m_bukkitTask.cancel();
            m_bukkitTask = null;
        }

        PluginConfiguration configuration = m_serverControl.getConfiguration();

        if(newState == GameState.CREATED)
        {
            String worldName = configuration.getString(PluginConfiguration.GAME_AREA_START_WORLD, "default");
            double centerX = configuration.getDouble(PluginConfiguration.GAME_AREA_START_CENTER_X, 0);
            double centerZ = configuration.getDouble(PluginConfiguration.GAME_AREA_START_CENTER_Z, 0);
            double startRadius = configuration.getDouble(PluginConfiguration.GAME_AREA_START_RADIUS, 16);

            s_logger.info(() -> "Set the border at " + centerX + "," + centerZ + " to the size of " + startRadius * 2 + " for " + worldName);
            setBorder(worldName, centerX, centerZ, startRadius * 2);
        }
        else if(newState == GameState.STANDBY)
        {
            s_logger.info(() -> "Set the border at 0,0 to the size of 30000000 for every world");

            for(World world : m_serverControl.getWorlds())
            {
                world.getWorldBorder().setCenter(0, 0);
                world.getWorldBorder().setSize(30000000);
            }
        }
        else
        {
            double centerX = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_CENTER_X, 0);
            double centerZ = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_CENTER_Z, 0);
            double radius = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_RADIUS, 1000);

            s_logger.info(() -> "Set the border to the running size of " + radius * 2 + " for every world!");
            setBorder((String)null, centerX, centerZ, radius * 2);

            boolean shrinkEnabled = configuration.getBoolean(PluginConfiguration.GAME_AREA_SHRINK_ENABLED, false);

            if(shrinkEnabled && newState == GameState.RUNNING)
            {
                long holdOffMinutes = configuration.getLong(PluginConfiguration.GAME_AREA_SHRINK_HOLD_OFF, 1);

                if(holdOffMinutes == 0)
                {
                    shrinkBorder(configuration);
                }
                else
                {
                    s_logger.info(() -> "Border shrinking will start in " + holdOffMinutes + " minutes!");

                    String message = ChatColor.GOLD + "Border shrinking will start in " + holdOffMinutes + " minutes!";
                    for(Player player : m_serverControl.getOnlinePlayers())
                    {
                        player.sendMessage(message);
                    }

                    m_bukkitTask = m_serverControl.getScheduler().runTaskDelayed(
                        () -> shrinkBorder(configuration),
                        PluginScheduler.s2t(holdOffMinutes * 60));
                }
            }
        }
    }

    private void shrinkBorder(PluginConfiguration configuration)
    {
        double shrinkRadius = configuration.getDouble(PluginConfiguration.GAME_AREA_SHRINK_RADIUS, 100);
        long minutes = configuration.getLong(PluginConfiguration.GAME_AREA_SHRINK_TIME, 10);

        s_logger.info(() -> "Shrinking the border size to " + shrinkRadius * 2 +
            " over the time of " + minutes + " minutes for every world!");

        changeBorderSize((String)null, shrinkRadius * 2, minutes * 60);

        String message = String.format(
            Locale.ENGLISH,
            ChatColor.GOLD + "Shrinking border over %d minutes to a radius of %.0f blocks!",
            minutes,
            shrinkRadius);

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            player.sendMessage(message);
        }

        Melody.BORDER_SHRINKING.play(
            m_serverControl.getScheduler(),
            m_serverControl.getOnlinePlayers(),
            Instrument.PIANO);
    }

    private void setBorder(String worldName, double centerX, double centerZ, double size)
    {
        World world = null;
        if(worldName != null)
        {
            world = m_serverControl.getWorld(worldName);
        }

        if(world == null)
        {
            for(World w : m_serverControl.getWorlds())
            {
                setBorder(w, centerX, centerZ, size);
            }
        }
        else
        {
            setBorder(world, centerX, centerZ, size);
        }
    }

    private void setBorder(World world, double centerX, double centerZ, double size)
    {
        if(world.getEnvironment() == World.Environment.NETHER)
        {
            centerX /= 8;
            centerZ /= 8;
        }

        world.getWorldBorder().setCenter(centerX, centerZ);
        world.getWorldBorder().setSize(size);
    }

    private void changeBorderSize(String worldName, double size, long seconds)
    {
        World world = null;
        if(worldName != null)
        {
            world = m_serverControl.getWorld(worldName);
        }

        if(world == null)
        {
            for(World w : m_serverControl.getWorlds())
            {
                changeBorderSize(w, size, seconds);
            }
        }
        else
        {
            changeBorderSize(world, size, seconds);
        }
    }

    private void changeBorderSize(World world, double size, long seconds)
    {
        world.getWorldBorder().setSize(size, seconds);
    }

    private BukkitTask m_bukkitTask;

    private final ServerControl m_serverControl;
}
