// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.Utilities;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scoreboard.Team;

import java.util.TimerTask;
import java.util.logging.Logger;


public class FireworksConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(FireworksConfiguration.class);

    // Take from https://minecraft.gamepedia.com/Formatting_codes
    private static final Color BLACK = Color.BLACK;
    private static final Color DARK_BLUE = Color.fromRGB(0x0000AA);
    private static final Color DARK_GREEN = Color.fromRGB(0x00AA00);
    private static final Color DARK_AQUA = Color.fromRGB(0x00AAAA);
    private static final Color DARK_RED = Color.fromRGB(0xAA0000);
    private static final Color DARK_PURPLE = Color.fromRGB(0xAA00AA);
    private static final Color GOLD = Color.fromRGB(0xFFAA00);
    private static final Color GRAY = Color.fromRGB(0xAAAAAA);
    private static final Color DARK_GRAY = Color.fromRGB(0x555555);
    private static final Color BLUE = Color.fromRGB(0x5555FF);
    private static final Color GREEN = Color.fromRGB(0x55FF55);
    private static final Color AQUA = Color.fromRGB(0x55FFFF);
    private static final Color RED = Color.fromRGB(0xFF5555);
    private static final Color LIGHT_PURPLE = Color.fromRGB(0xFF55FF);
    private static final Color YELLOW = Color.fromRGB(0xFFFF55);
    private static final Color WHITE = Color.WHITE;

    private static final long DEFAULT_FIREWORKS_HOLD_OFF = 0;
    private static final long DEFAULT_FIREWORKS_EVERY = 8;
    private static final long DEFAULT_FIREWORKS_POWER = 3;

    public FireworksConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(m_runningTimer != null)
        {
            m_runningTimer.cancel();
            m_runningTimer = null;
        }

        if(!isFireworksEnabled())
        {
            return;
        }

        if(newState == GameState.RUNNING)
        {
            long holdOff = getFireworksHoldOff();
            long every = getFireworksEvery();

            PluginScheduler scheduler = m_serverControl.getScheduler();
            m_runningTimer = scheduler.scheduleRepeated(this::fireFireworks, holdOff, every);

            s_logger.info(() -> "Starting fireworks after " + (holdOff / 1000 / 60)
                + " minutes and fire every " + (every / 1000 / 60) + " minutes afterwards!");
        }
    }

    private boolean isFireworksEnabled()
    {
        return m_serverControl.getConfiguration().getBoolean(
            PluginConfiguration.GAME_FIREWORKS_ENABLED,
            false);
    }

    private int getFireworksPower()
    {
        return (int)m_serverControl.getConfiguration().getLong(
            PluginConfiguration.GAME_FIREWORKS_POWER,
            DEFAULT_FIREWORKS_POWER);
    }

    private long getFireworksHoldOff()
    {
        long durationInMinutes = m_serverControl.getConfiguration().getLong(
            PluginConfiguration.GAME_FIREWORKS_HOLD_OFF,
            DEFAULT_FIREWORKS_HOLD_OFF);

        return durationInMinutes * 60 * 1000;
    }

    private long getFireworksEvery()
    {
        long durationInMinutes = m_serverControl.getConfiguration().getLong(
            PluginConfiguration.GAME_FIREWORKS_EVERY,
            DEFAULT_FIREWORKS_EVERY);

        return durationInMinutes * 60 * 1000;
    }

    private void fireFireworks()
    {
        s_logger.info(() -> "Firing fireworks! Woo! Fireworks! Oh shit, hide, hide hide! Run away!!!");

        int fireworkPower = getFireworksPower();

        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(GameModeConfiguration.isAlive(player))
            {
                Location fireLocation = Utilities.findBlockWithDirectSkyView(player.getLocation());

                World world = fireLocation.getWorld();
                world.spawn(fireLocation, Firework.class, firework -> {
                    FireworkMeta fireworkMeta = firework.getFireworkMeta();
                    fireworkMeta.addEffect(FireworkEffect.builder()
                        .withColor(getTeamColor(player))
                        .with(FireworkEffect.Type.CREEPER)
                        .withFlicker()
                        .withTrail()
                        .withFade(Color.WHITE)
                        .build());

                    fireworkMeta.setPower(fireworkPower);
                    firework.setFireworkMeta(fireworkMeta);
                });
            }
        }
    }

    private Color getTeamColor(Player player)
    {
        Team team = m_serverControl.getMainScoreboard().getEntryTeam(player.getName());
        if(team == null)
        {
            return Color.WHITE;
        }

        switch(team.getColor())
        {
        case RED:
            return RED;
        case GOLD:
            return GOLD;
        case GREEN:
            return GREEN;
        case YELLOW:
            return YELLOW;
        case AQUA:
            return AQUA;
        case BLUE:
            return BLUE;
        case GRAY:
            return GRAY;
        case BLACK:
            return BLACK;
        case DARK_RED:
            return DARK_RED;
        case DARK_AQUA:
            return DARK_AQUA;
        case DARK_BLUE:
            return DARK_BLUE;
        case DARK_GRAY:
            return DARK_GRAY;
        case DARK_GREEN:
            return DARK_GREEN;
        case DARK_PURPLE:
            return DARK_PURPLE;
        case LIGHT_PURPLE:
            return LIGHT_PURPLE;
        case WHITE:
        case MAGIC:
        default:
            return WHITE;
        }
    }

    private final ServerControl m_serverControl;
    private TimerTask m_runningTimer;
}
