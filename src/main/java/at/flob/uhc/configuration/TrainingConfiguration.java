// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import at.flob.uhc.Utilities;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;


public class TrainingConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(TrainingConfiguration.class);

    private static final Set<UUID> TRAINEES = new HashSet<>();
    private static boolean s_trainingIsActive = false;

    public static void setTrainee(Player player, boolean isTrainee)
    {
        if(isTrainee)
        {
            TRAINEES.add(player.getUniqueId());
        }
        else
        {
            TRAINEES.remove(player.getUniqueId());
        }
    }

    public static boolean isTrainee(Player player)
    {
        return TRAINEES.contains(player.getUniqueId());
    }

    public static void equipPlayer(Player player)
    {
        PlayerInventory inventory = player.getInventory();

        inventory.setHelmet(new ItemStack(Material.IRON_HELMET));
        inventory.setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
        inventory.setLeggings(new ItemStack(Material.IRON_LEGGINGS));
        inventory.setBoots(new ItemStack(Material.IRON_BOOTS));

        inventory.setItemInOffHand(new ItemStack(Material.SHIELD));

        inventory.setItem(0, new ItemStack(Material.IRON_SWORD));
        inventory.setItem(1, new ItemStack(Material.IRON_AXE));

        ItemStack hoe = new ItemStack(Material.IRON_HOE);
        Utilities.renameItemStack(hoe, "His Hoe");
        inventory.setItem(2, hoe);

        ItemStack spade = new ItemStack(Material.IRON_SPADE);
        Utilities.renameItemStack(spade, "Grave Digger");
        inventory.setItem(3, spade);

        ItemStack bow = new ItemStack(Material.BOW);
        bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        inventory.setItem(5, bow);
        inventory.setItem(6, new ItemStack(Material.ARROW, 1));

        inventory.setItem(8, new ItemStack(Material.GOLDEN_CARROT, 64));

        player.addPotionEffect(
            new PotionEffect(
                PotionEffectType.DAMAGE_RESISTANCE,
                (int)PluginScheduler.s2t(10),
                1,
                true,
                true),
            true);
    }

    public static boolean isDeactivated()
    {
        return !s_trainingIsActive;
    }

    public TrainingConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
        m_serverControl.registerEventListener(new Listener()
        {
            @EventHandler
            public void onPlayerDeath(PlayerDeathEvent event)
            {
                TrainingConfiguration.this.onPlayerDeath(event);
            }

            @EventHandler
            public void onPlayerRespawn(PlayerRespawnEvent event)
            {
                TrainingConfiguration.this.onPlayerRespawn(event);
            }
        });
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(newState == GameState.CREATED)
        {
            boolean isEnabled = getConfiguration().getBoolean(
                PluginConfiguration.GAME_AREA_TRAINING_ENABLED,
                false);

            if(!isEnabled)
            {
                return;
            }

            String startWorld = getConfiguration().getString(PluginConfiguration.GAME_AREA_START_WORLD, "world");
            String trainingWorld = getConfiguration().getString(PluginConfiguration.GAME_AREA_TRAINING_WORLD, "world");

            if(startWorld.equalsIgnoreCase(trainingWorld))
            {
                s_logger.warning(() -> "Training disabled since the start and training worlds are the same!");
                return;
            }

            World world = m_serverControl.getWorld(trainingWorld);
            if(world == null)
            {
                s_logger.warning(() -> "The training world '" + trainingWorld + "' wasn't found on this server!");
                return;
            }

            world.setPVP(true);
            Utilities.setHealthRegeneration(world, false);
            WorldBorder border = world.getWorldBorder();

            double centerX = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_CENTER_X, 0);
            double centerZ = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_CENTER_Z, 0);
            double radius = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_RADIUS, 100);

            border.setCenter(centerX, centerZ);
            border.setSize(radius * 2);

            s_logger.info(() -> "Training activated!");
            s_trainingIsActive = true;
        }
        else
        {
            s_logger.info(() -> "Training disabled!");
            s_trainingIsActive = false;
            TRAINEES.clear();
        }
    }

    private void onPlayerDeath(PlayerDeathEvent event)
    {
        if(isDeactivated())
        {
            return;
        }

        Player player = event.getEntity();

        if(isTrainee(player))
        {
            event.getDrops().clear();
            event.getDrops().add(new ItemStack(Material.GOLDEN_APPLE, 1));
            event.setDroppedExp(0);
        }
    }

    private void onPlayerRespawn(PlayerRespawnEvent event)
    {
        if(isDeactivated())
        {
            return;
        }

        equipPlayer(event.getPlayer());
    }

    private PluginConfiguration getConfiguration()
    {
        return m_serverControl.getConfiguration();
    }

    private final ServerControl m_serverControl;
}
