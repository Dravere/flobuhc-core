// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;

import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class HealthRegenerationConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(HealthRegenerationConfiguration.class);

    HealthRegenerationConfiguration(ServerControl serverControl)
    {
        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        boolean enableHealthRegen = (newState != GameState.RUNNING);
        s_logger.info(() -> enableHealthRegen ? "Enabling health regeneration!" : "Disabling health regeneration!");
        m_serverControl.setHealthRegeneration(enableHealthRegen);
    }

    private ServerControl m_serverControl;
}
