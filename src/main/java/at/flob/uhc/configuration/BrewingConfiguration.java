// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class BrewingConfiguration
    implements GameStateListener
{
    private static final Logger s_logger = LoggerFactory.getLogger(BrewingConfiguration.class);

    BrewingConfiguration(ServerControl serverControl)
    {
        m_forbiddenPotionRecipes = new ArrayList<>(2);
        m_forbiddenPotionRecipes.add(new PotionRecipe(new PotionData(PotionType.STRENGTH), Material.GLOWSTONE_DUST));
        m_forbiddenPotionRecipes.add(new PotionRecipe(new PotionData(PotionType.AWKWARD), Material.GHAST_TEAR));

        serverControl.registerEventListener(new Listener()
        {
            @EventHandler
            public void onBrewEvent(BrewEvent event)
            {
                BrewingConfiguration.this.onBrewEvent(event);
            }
        });
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        m_enforceRestrictions = newState != GameState.STANDBY;
    }

    private void onBrewEvent(BrewEvent event)
    {
        if(!m_enforceRestrictions)
        {
            return;
        }

        BrewerInventory inventory = event.getContents();
        ItemStack ingredient = inventory.getIngredient();

        ArrayList<PotionRecipe> potionRecipes = new ArrayList<>(m_forbiddenPotionRecipes.size());
        for(PotionRecipe recipe : m_forbiddenPotionRecipes)
        {
            if(recipe.isIngredient(ingredient))
            {
                potionRecipes.add(recipe);
            }
        }

        for(ItemStack itemStack : inventory.getContents())
        {
            if(itemStack == null)
            {
                continue;
            }

            for(PotionRecipe recipe : potionRecipes)
            {
                if(recipe.isBasePotion(itemStack))
                {
                    for(HumanEntity viewers : inventory.getViewers())
                    {
                        viewers.sendMessage(ChatColor.RED + "This type of potion is disabled!");
                    }

                    s_logger.info(() -> "Potion brewing was cancelled: " + itemStack + " + " + ingredient);
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    private boolean m_enforceRestrictions;
    private final Collection<PotionRecipe> m_forbiddenPotionRecipes;
}
