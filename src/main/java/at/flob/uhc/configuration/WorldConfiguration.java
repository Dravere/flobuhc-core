// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class WorldConfiguration
    implements GameStateListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WorldConfiguration.class);

    public WorldConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        serverControl.registerEventListener(new Listener()
        {
            @EventHandler
            public void onPlayerJoin(PlayerJoinEvent event)
            {
                m_serverControl.getScheduler().runTaskDelayed(
                    () -> WorldConfiguration.this.onPlayerJoin(event),
                    1);
            }
        });

        m_serverControl = serverControl;
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        if(newState == GameState.CREATED)
        {
            setSendToLocation(
                PluginConfiguration.GAME_AREA_START_WORLD,
                PluginConfiguration.GAME_AREA_START_CENTER_X,
                PluginConfiguration.GAME_AREA_START_CENTER_Y,
                PluginConfiguration.GAME_AREA_START_CENTER_Z);
        }
        else
        {
            m_sendToLocation = null;
        }
    }

    private void setSendToLocation(
        String configPathWorld,
        String configPathCenterX,
        String configPathCenterY,
        String configPathCenterZ)
    {
        PluginConfiguration config = m_serverControl.getConfiguration();
        String worldName = config.getString(configPathWorld, "world");
        World world = m_serverControl.getWorld(worldName);

        if(world == null)
        {
            LOGGER.warning(() -> "Unable to find the world with the name: " + worldName + "!");
            m_sendToLocation = null;
            return;
        }

        double centerX = config.getDouble(configPathCenterX, 0);
        double centerY = config.getDouble(configPathCenterY, Double.NaN);
        double centerZ = config.getDouble(configPathCenterZ, 0);

        if(Double.isNaN(centerY))
        {
            centerY = world.getHighestBlockYAt((int)centerX, (int)centerZ);
        }

        m_sendToLocation = new Location(world, centerX, centerY, centerZ);
    }

    private void onPlayerJoin(PlayerJoinEvent event)
    {
        if(m_sendToLocation == null)
        {
            return;
        }

        // TODO: What is still missing is keeping spectators in the right worlds

        Player player = event.getPlayer();
        if(!TrainingConfiguration.isTrainee(player) &&
            isOutsideBorder(player.getLocation(), m_sendToLocation.getWorld()))
        {
            LOGGER.info(() -> "Teleporting player " + player.getName() + " to " + m_sendToLocation);

            if(!player.teleport(m_sendToLocation))
            {
                LOGGER.warning(() -> "Teleporting player " + player.getName() + " failed!");
            }
        }
    }

    private boolean isOutsideBorder(Location location, World world)
    {
        if(location.getWorld() != world)
        {
            return true;
        }

        WorldBorder border = world.getWorldBorder();
        double size = border.getSize();
        Location center = border.getCenter();

        return location.getX() < (center.getX() - size) || location.getX() > (center.getX() + size)
            || location.getZ() < (center.getZ() - size) || location.getZ() > (center.getZ() + size);
    }

    private Location m_sendToLocation;
    private final ServerControl m_serverControl;
}
