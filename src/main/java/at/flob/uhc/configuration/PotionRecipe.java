// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;


/**
 * @author Florian Graf
 */
class PotionRecipe
{
    public PotionRecipe(PotionData basePotion, Material ingredient)
    {
        m_ingredient = ingredient;
        m_basePotion = basePotion;
    }

    public boolean isIngredient(ItemStack itemStack)
    {
        return itemStack != null && itemStack.getType() == m_ingredient;
    }

    public boolean isBasePotion(ItemStack itemStack)
    {
        if(itemStack != null && itemStack.getType() == Material.POTION)
        {
            PotionMeta potionMeta = (PotionMeta)itemStack.getItemMeta();
            return potionMeta.getBasePotionData().equals(m_basePotion);
        }

        return false;
    }

    private final Material m_ingredient;
    private final PotionData m_basePotion;
}
