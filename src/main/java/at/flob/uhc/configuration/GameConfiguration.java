// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.configuration;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateListener;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Florian Graf
 */
public class GameConfiguration
    implements GameStateListener
{
    public GameConfiguration(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control can't be null!");
        }

        m_configurations = new ArrayList<>();
        m_configurations.add(new BorderConfiguration(serverControl));
        m_configurations.add(new BrewingConfiguration(serverControl));
        m_configurations.add(new GameModeConfiguration(serverControl));
        m_configurations.add(new HealthRegenerationConfiguration(serverControl));
        m_configurations.add(new ObjectiveConfiguration(serverControl));
        m_configurations.add(new EpisodeTimerConfiguration(serverControl));
        m_configurations.add(new CombatConfiguration(serverControl));
        m_configurations.add(new DifficultyConfiguration(serverControl));
        m_configurations.add(new WorldConfiguration(serverControl));
        m_configurations.add(new CageConfiguration(serverControl));
        m_configurations.add(new DaylightCycleConfiguration(serverControl));
        m_configurations.add(m_appliedEffectsConfiguration = new AppliedEffectsConfiguration(serverControl));
        m_configurations.add(new FireworksConfiguration(serverControl));
        m_configurations.add(new TrainingConfiguration(serverControl));
    }

    @Override
    public void onGameStateChanged(GameState previousState, GameState newState)
    {
        for(GameStateListener listener : m_configurations)
        {
            listener.onGameStateChanged(previousState, newState);
        }
    }

    public AppliedEffectsConfiguration getAppliedEffectsConfiguration()
    {
        return m_appliedEffectsConfiguration;
    }

    private final List<GameStateListener> m_configurations;
    private final AppliedEffectsConfiguration m_appliedEffectsConfiguration;
}
