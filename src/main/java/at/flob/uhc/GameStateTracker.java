// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class GameStateTracker
{
    private static final Logger s_logger = LoggerFactory.getLogger(GameStateTracker.class);

    /**
     * Initializes a game state tracker with the initial state set to {@link GameState#STANDBY}.
     */
    public GameStateTracker()
    {
        m_gameStateListeners = new ArrayList<>();
        m_currentState = GameState.STANDBY;
    }

    /**
     * Gets the current state of this tracker.
     *
     * @return The current state of this tracker.
     */
    public GameState getCurrentState()
    {
        return m_currentState;
    }

    /**
     * Switches into the given state if possible.
     *
     * @return If it was possible to switch into the given state true; otherwise false.
     */
    public boolean tryChangeState(GameState newState)
    {
        switch(m_currentState)
        {
        case STANDBY:
            if(newState == GameState.CREATED)
            {
                changeState(newState);
                return true;
            }
            break;
        case CREATED:
            if(newState == GameState.READY || newState == GameState.STANDBY)
            {
                changeState(newState);
                return true;
            }
            break;
        case READY:
            if(newState == GameState.RUNNING || newState == GameState.CREATED  || newState == GameState.STANDBY)
            {
                changeState(newState);
                return true;
            }
            break;
        case RUNNING:
            if(newState == GameState.FINISHED)
            {
                changeState(newState);
                return true;
            }
            break;
        case FINISHED:
            if(newState == GameState.STANDBY)
            {
                changeState(newState);
                return true;
            }
            break;
        }

        s_logger.warning(() -> "Failed to change the game state to: " + newState);
        return false;
    }

    /**
     * Adds the given listener if it isn't null.
     *
     * @param listener The listener to be added. Will be ignored if it is null.
     */
    public void addGameStateListener(GameStateListener listener)
    {
        if(listener != null)
        {
            m_gameStateListeners.add(listener);
        }
    }

    /**
     * Removes the given listener.
     *
     * @param listener Removes the given listener. Will be ignored if null.
     */
    public void removeGameStateListener(GameStateListener listener)
    {
        m_gameStateListeners.remove(listener);
    }

    /**
     * Changes the current state of the tracker and fires a game state changed event to all listener.
     *
     * @param newState The new state the tracker should be in.
     */
    private void changeState(GameState newState)
    {
        s_logger.info(() -> "Changing game state to: " + newState);
        GameState previousState = m_currentState;
        m_currentState = newState;
        fireGameStateChange(previousState, newState);
        s_logger.info(() -> "Changed game state to: " + newState);
    }

    /**
     * Sends a game state changed to all registered listener of this tracker.
     *
     * @param previousState The previously set game state to be sent.
     * @param newState The newly set game state to be sent.
     */
    private void fireGameStateChange(GameState previousState, GameState newState)
    {
        for(GameStateListener listener : m_gameStateListeners)
        {
            listener.onGameStateChanged(previousState, newState);
        }
    }

    private final ArrayList<GameStateListener> m_gameStateListeners;

    private GameState m_currentState;
}
