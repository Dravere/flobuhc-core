// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author Florian Graf
 */
public class Melody
{
    static class Step
    {
        Step(long delay, float pitch, String message)
        {
            this.pitch = pitch;
            this.delay = delay;
            this.message = message;
        }

        final float pitch;
        final long delay;
        final String message;
    }

    public static final Melody GAME_START = new Melody();
    public static final Melody PVP_ENABLED = new Melody();
    public static final Melody BORDER_SHRINKING = new Melody();
    public static final Melody EPISODE_ENDING = new Melody();

    static
    {
        GAME_START
            .natural(0, 0, Note.Tone.A, ChatColor.GOLD + "3...")
            .natural(1000, 0, Note.Tone.A, ChatColor.GOLD + "2...")
            .natural(1000, 0, Note.Tone.A, ChatColor.GOLD + "1...")
            .natural(1000, 1, Note.Tone.A, ChatColor.GOLD + "Go! Have fun!");

        PVP_ENABLED
            .natural(0, 0, Note.Tone.G)
            .natural(300, 0, Note.Tone.G)
            .natural(300, 0, Note.Tone.G)
            .sharp(300, 0, Note.Tone.D);

        BORDER_SHRINKING
            .natural(0, 0, Note.Tone.G)
            .natural(500, 0, Note.Tone.E)
            .natural(500, 0, Note.Tone.C)
            .natural(500, 0, Note.Tone.A);

        EPISODE_ENDING
            .natural(0, 1, Note.Tone.G)
            .natural(300, 1, Note.Tone.G)
            .natural(300, 1, Note.Tone.G);
    }

    private static float convertNoteToPitch(Note note)
    {
        double key = note.getOctave() * 12;
        switch(note.getTone())
        {
        case A:
            break;
        case B:
            key += 2;
            break;
        case C:
            key += 3;
            break;
        case D:
            key += 5;
            break;
        case E:
            key += 7;
            break;
        case F:
            key += 8;
            break;
        case G:
            key += 10;
            break;
        }

        if(note.isSharped())
        {
            key += 1;
        }

        return (float)key / 12;
    }

    public Melody()
    {
        m_steps = new ArrayList<>();
    }

    public Melody append(long delayMilliseconds, Note note)
    {
        return append(delayMilliseconds, note, null);
    }

    public Melody append(long delayMilliseconds, Note note, String message)
    {
        m_steps.add(new Step(PluginScheduler.ms2t(delayMilliseconds), convertNoteToPitch(note), message));
        return this;
    }

    public Melody natural(long delayMilliseconds, int octave, Note.Tone tone)
    {
        return natural(delayMilliseconds, octave, tone, null);
    }

    public Melody natural(long delayMilliseconds, int octave, Note.Tone tone, String message)
    {
        return append(delayMilliseconds, Note.natural(octave, tone), message);
    }

    public Melody sharp(long delayMilliseconds, int octave, Note.Tone tone)
    {
        return sharp(delayMilliseconds, octave, tone, null);
    }

    public Melody sharp(long delayMilliseconds, int octave, Note.Tone tone, String message)
    {
        return append(delayMilliseconds, Note.sharp(octave, tone), message);
    }

    public MelodyPlay play(PluginScheduler scheduler, Collection<? extends Player> playerList, Instrument instrument)
    {
        Step[] steps = m_steps.toArray(new Step[m_steps.size()]);
        Player[] players = playerList.toArray(new Player[playerList.size()]);

        Sound sound;
        switch(instrument)
        {
        default:
        case PIANO:
            sound = Sound.BLOCK_NOTE_PLING;
            break;
        case STICKS:
            sound = Sound.BLOCK_NOTE_HAT;
            break;
        case BASS_DRUM:
            sound = Sound.BLOCK_NOTE_BASEDRUM;
            break;
        case SNARE_DRUM:
            sound = Sound.BLOCK_NOTE_SNARE;
            break;
        case BASS_GUITAR:
            sound = Sound.BLOCK_NOTE_BASS;
        }

        return new MelodyPlay(scheduler, sound, players, steps);
    }

    private final List<Step> m_steps;
}
