// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import at.flob.uhc.actions.*;
import at.flob.uhc.commands.*;
import at.flob.uhc.configuration.GameConfiguration;
import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Florian Graf
 */
class UltraHardcore
{
    private static final String COMMAND_CREATE = "CREATE";
    private static final String COMMAND_READY = "READY";
    private static final String COMMAND_START = "START";
    private static final String COMMAND_FINISH = "FINISH";
    private static final String COMMAND_EXIT = "EXIT";

    private static final String COMMAND_JOIN = "JOIN";
    private static final String COMMAND_TEAM = "TEAM";
    private static final String COMMAND_SPECTATE = "SPECTATE";
    private static final String COMMAND_NIGHT_VISION = "NIGHTVISION";
    private static final String COMMAND_LIST = "LIST";
    private static final String COMMAND_TRAINING = "TRAINING";

    private static final String COMMAND_CONFIG = "CONFIG";
    private static final String COMMAND_REVIVE = "REVIVE";

    private static final int DELAYED_START_MS = 3000;

    private static File s_dataFolder = new File("./plugins/FLoB-UHC-Core/");

    static void setDataFolder(File folder)
    {
        if(folder == null)
        {
            throw new IllegalArgumentException("The folder file cannot be null!");
        }

        s_dataFolder = folder;
    }

    public static File getDataFolder()
    {
        return s_dataFolder;
    }

    public UltraHardcore(ServerControl serverControl)
    {
        TeamManager teamManager = new TeamManager(serverControl);

        GameStateTracker gameStateTracker = new GameStateTracker();

        GameConfiguration gameConfiguration = new GameConfiguration(serverControl);
        gameStateTracker.addGameStateListener(gameConfiguration);

        gameStateTracker.addGameStateListener(new GatherPlayerAction(serverControl, GameState.STANDBY));

        gameStateTracker.addGameStateListener(new GatherPlayerAction(serverControl, GameState.CREATED));
        gameStateTracker.addGameStateListener(new ClearTeamsAction(teamManager, GameState.CREATED));

        gameStateTracker.addGameStateListener(new SpreadPlayerAction(serverControl, GameState.READY));
        gameStateTracker.addGameStateListener(new ClearInventoryAction(serverControl, GameState.READY));

        gameStateTracker.addGameStateListener(new SaveConfigurationAction(
            serverControl.getConfiguration(),
            GameState.READY));

        gameStateTracker.addGameStateListener(new SetSaturationAction(
            serverControl,
            5.0f,
            GameState.RUNNING));

        m_commands = new HashMap<>();
        m_commands.put(COMMAND_CREATE, new GameStateChangeCommand(gameStateTracker, GameState.CREATED));
        m_commands.put(COMMAND_READY, new GameStateChangeCommand(gameStateTracker, GameState.READY));

        m_commands.put(COMMAND_START, new DelayedGameStateChangeCommand(
            serverControl,
            gameStateTracker,
            GameState.RUNNING,
            DELAYED_START_MS));

        m_commands.put(COMMAND_FINISH, new GameStateChangeCommand(gameStateTracker, GameState.FINISHED));
        m_commands.put(COMMAND_EXIT, new GameStateChangeCommand(gameStateTracker, GameState.STANDBY));

        m_commands.put(COMMAND_JOIN, new TeamJoinCommand(teamManager, gameStateTracker));
        m_commands.put(COMMAND_TEAM, new TeamCommand(serverControl, teamManager, gameStateTracker));
        m_commands.put(COMMAND_SPECTATE, new ToggleSpectatorCommand(gameStateTracker));
        m_commands.put(COMMAND_NIGHT_VISION, new NightVisionCommand());
        m_commands.put(COMMAND_LIST, new ListCommand(serverControl));
        m_commands.put(COMMAND_TRAINING, new TrainingCommand(
            serverControl,
            gameStateTracker,
            gameConfiguration.getAppliedEffectsConfiguration()));

        m_commands.put(COMMAND_CONFIG, new ConfigCommand(gameStateTracker, serverControl.getConfiguration()));
        m_commands.put(COMMAND_REVIVE, new ReviveCommand(serverControl));
    }

    public void enable()
    {
        // Do we do anything in here?
    }

    public void disable()
    {
        // TODO Should exit game state into standby!
    }

    public boolean runCommand(CommandSender sender, String label, CommandArguments arguments)
    {
        Command command = m_commands.get(label.toUpperCase());

        if(command == null)
        {
            sender.sendMessage(ChatColor.RED + "Unknown command: " + label);
            return true;
        }

        String requiredPermission = command.getRequiredPermission();
        if(requiredPermission != null && !sender.hasPermission(requiredPermission))
        {
            sender.sendMessage(ChatColor.RED + "You don't have permissions to run this command!");
            return true;
        }

        command.execute(sender, arguments);
        return true;
    }

    public List<String> getTabCompleteList(CommandSender sender, String label, CommandArguments arguments)
    {
        if(label == null)
        {
            return Lists.newArrayList(m_commands.keySet());
        }
        else if(arguments.getLength() == 0)
        {
            List<String> result = new ArrayList<>();
            for(String commandKey : m_commands.keySet())
            {
                String requiredPermission = m_commands.get(commandKey).getRequiredPermission();

                if((requiredPermission == null || sender.hasPermission(requiredPermission))
                    && commandKey.startsWith(label.toUpperCase()))
                {
                    result.add(commandKey.toLowerCase());
                }
            }

            return result;
        }
        else
        {
            Command command = m_commands.get(label.toUpperCase());

            if(command == null)
            {
                return new ArrayList<>();
            }

            String requiredPermission = command.getRequiredPermission();
            if(requiredPermission != null && !sender.hasPermission(requiredPermission))
            {
                return new ArrayList<>();
            }

            List<String> result = command.getTabCompleteList(sender, arguments);
            if(result == null)
            {
                result = new ArrayList<>();
            }

            return result;
        }
    }

    private final Map<String, Command> m_commands;
}
