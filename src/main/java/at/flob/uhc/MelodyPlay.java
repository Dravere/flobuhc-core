// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;


/**
 * @author Florian Graf
 */
public class MelodyPlay
{
    MelodyPlay(PluginScheduler scheduler, Sound sound, Player[] players, Melody.Step[] steps)
    {
        m_currentStep = 0;

        m_scheduler = scheduler;
        m_sound = sound;
        m_players = players;
        m_steps = steps;

        scheduleStep();
    }

    private void playStep()
    {
        Melody.Step step = m_steps[m_currentStep];
        for(Player player : m_players)
        {
            if(player.isOnline())
            {
                player.playSound(player.getLocation(), m_sound, SoundCategory.RECORDS, 1.0f, step.pitch);

                if(step.message != null)
                {
                    player.sendMessage(step.message);
                }
            }
        }

        ++m_currentStep;
        scheduleStep();
    }

    private void scheduleStep()
    {
        if(m_currentStep == m_steps.length)
        {
            return;
        }

        Melody.Step step = m_steps[m_currentStep];

        if(step.delay == 0)
        {
            playStep();
        }
        else
        {
            m_scheduler.runTaskDelayed(this::playStep, step.delay);
        }
    }

    private int m_currentStep;

    private final PluginScheduler m_scheduler;
    private final Sound m_sound;
    private final Player[] m_players;
    private final Melody.Step[] m_steps;
}
