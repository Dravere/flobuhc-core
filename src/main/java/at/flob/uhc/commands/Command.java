// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import org.bukkit.command.CommandSender;

import java.util.List;


/**
 * A command that can be executed with the given arguments.
 *
 * @author Florian Graf
 */
public interface Command
{
    String PERMISSION_OPERATOR = "flobuhc.operator";
    String PERMISSION_PLAYER = "flobuhc.player";

    /**
     * Executes the command with the given arguments.
     *
     * @param sender The sender of the command. This isn't always a player.
     * @param arguments The arguments to be used for the execution.
     *
     * @throws IllegalArgumentException If arguments is null.
     */
    void execute(CommandSender sender, CommandArguments arguments);

    /**
     * Get the list of string to present to the user with the currently provided arguments when he presses tab. The last
     * argument can be empty if he hasn't provided one yet.
     *
     * @param sender The sender of the command. This isn't always a player.
     * @param arguments The arguments currently provided. The last argument can be an empty string.
     *
     * @return A list of strings that could be provided as the last argument.
     *
     * @throws IllegalArgumentException If sender or arguments is null.
     */
    List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments);

    /**
     * Gets the required permission.
     *
     * @return The required permission.
     */
    String getRequiredPermission();
}
