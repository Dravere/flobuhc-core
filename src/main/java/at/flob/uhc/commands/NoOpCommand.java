// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import org.bukkit.command.CommandSender;

import java.util.List;


/**
 * This command does nothing.
 *
 * @author Florian Graf
 */
public class NoOpCommand
    implements Command
{
    public static final Command INSTANCE = new NoOpCommand();

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        // Does nothing
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        return null;
    }

    @Override
    public String getRequiredPermission()
    {
        return null;
    }
}
