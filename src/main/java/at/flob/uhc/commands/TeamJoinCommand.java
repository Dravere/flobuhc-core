// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.TeamManager;
import at.flob.uhc.configuration.GameModeConfiguration;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class TeamJoinCommand
    implements Command
{
    private final static Logger s_logger = LoggerFactory.getLogger(TeamJoinCommand.class);

    public TeamJoinCommand(TeamManager teamManager, GameStateTracker gameStateTracker)
    {
        if(teamManager == null)
        {
            throw new IllegalArgumentException("The team manager cannot be null!");
        }

        if(gameStateTracker == null)
        {
            throw new IllegalArgumentException("The game state tracker cannot be null!");
        }

        m_teamManager = teamManager;
        m_gameStateTracker = gameStateTracker;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(!(sender instanceof Player))
        {
            s_logger.info(() -> "Received flobuhc team join command for non-player: " + sender);
            return;
        }

        if(m_gameStateTracker.getCurrentState() != GameState.CREATED)
        {
            sender.sendMessage(ChatColor.RED + "You can join a team only during game setup!");
            return;
        }

        if(arguments.getLength() < 1)
        {
            sender.sendMessage(ChatColor.RED + "You need to specify a team you want to join!");
            return;
        }

        String teamName = arguments.getArgument(0).trim();
        if(teamName.length() == 0)
        {
            sender.sendMessage(ChatColor.RED + "The team name can't be empty or consist of whitespace only!");
            return;
        }
        else if(teamName.length() > 16)
        {
            sender.sendMessage(ChatColor.RED + "The team name can't be longer than 16 characters!");
            return;
        }

        Player player = (Player)sender;
        if(GameModeConfiguration.isSpectating(player))
        {
            sender.sendMessage(ChatColor.RED + "You are no longer spectating!");
            GameModeConfiguration.cancelSpectating(player);
        }

        m_teamManager.joinTeam(player, teamName);
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> result = new ArrayList<>();

        for(String teamName : m_teamManager.getTeamNames())
        {
            if(teamName.startsWith(arguments.getArgument(0)))
            {
                result.add(teamName);
            }
        }

        return result;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }

    private final TeamManager m_teamManager;
    private final GameStateTracker m_gameStateTracker;
}
