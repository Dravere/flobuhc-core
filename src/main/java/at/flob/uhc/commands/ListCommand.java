// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.ServerControl;
import at.flob.uhc.configuration.GameModeConfiguration;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class ListCommand
    implements Command
{
    private static final String LIST_TYPE_TEAMS = "TEAMS";
    private static final String LIST_TYPE_SPECTATORS = "SPECTATORS";

    public ListCommand(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(arguments.getLength() < 1)
        {
            sender.sendMessage(ChatColor.RED + "Invalid command, expected teams got nothing!");
            return;
        }

        String listType = arguments.getArgument(0);

        switch(listType.toUpperCase())
        {
        case LIST_TYPE_TEAMS:
            listTeams(sender);
            break;
        case LIST_TYPE_SPECTATORS:
            listSpectators(sender);
            break;
        default:
            sender.sendMessage(ChatColor.RED + "Unknown list type: " + listType);
            break;
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> results = new ArrayList<>();
        String listType = arguments.getArgument(0).toUpperCase();

        if(LIST_TYPE_TEAMS.startsWith(listType))
        {
            results.add(LIST_TYPE_TEAMS.toLowerCase());
        }

        if(LIST_TYPE_SPECTATORS.startsWith(listType))
        {
            results.add(LIST_TYPE_SPECTATORS.toLowerCase());
        }

        return results;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }

    private void listTeams(CommandSender sender)
    {
        Set<Team> teams = m_serverControl.getMainScoreboard().getTeams();

        if(teams.isEmpty())
        {
            sender.sendMessage("No teams available yet!");
        }
        else
        {
            for(Team team : teams)
            {
                sender.sendMessage(team.getName() + ": " + String.join(", ", team.getEntries()));
            }
        }
    }

    private void listSpectators(CommandSender sender)
    {
        List<String> spectators = new ArrayList<String>();
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(GameModeConfiguration.isSpectating(player))
            {
                spectators.add(player.getName());
            }
        }

        if(spectators.isEmpty())
        {
            sender.sendMessage("No spectators yet!");
        }
        else
        {
            sender.sendMessage(String.join(", ", spectators));
        }
    }

    private final ServerControl m_serverControl;
}
