// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.ServerControl;
import at.flob.uhc.configuration.GameModeConfiguration;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Florian Graf
 */
public class ReviveCommand
    implements Command
{
    public ReviveCommand(ServerControl serverControl)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(arguments.getLength() < 1)
        {
            sender.sendMessage(ChatColor.RED + "Need to give the name of the player!");
            return;
        }

        String playerName = arguments.getArgument(0);
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(player.getName().equalsIgnoreCase(playerName))
            {
                GameModeConfiguration.markPlayerAlive(player);
                break;
            }
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> result = new ArrayList<>();

        String playerName = arguments.getArgument(0);
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(!GameModeConfiguration.isAlive(player) && player.getName().startsWith(playerName))
            {
                result.add(player.getName());
            }
        }

        return result;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_OPERATOR;
    }

    private ServerControl m_serverControl;
}
