// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.configuration.GameModeConfiguration;
import at.flob.uhc.configuration.PlayerMode;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;


/**
 * @author Florian Graf
 */
public class ToggleSpectatorCommand
    implements Command
{
    public ToggleSpectatorCommand(GameStateTracker gameStateTracker)
    {
        if(gameStateTracker == null)
        {
            throw new IllegalArgumentException("The game state tracker cannot be null!");
        }

        m_gameStateTracker = gameStateTracker;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("You are not a player!");
            return;
        }

        if(m_gameStateTracker.getCurrentState() != GameState.CREATED)
        {
            sender.sendMessage(ChatColor.YELLOW + "You can only change spectator mode during game setup!");
            return;
        }

        if(GameModeConfiguration.toggleSpectator((Player)sender) == PlayerMode.SPECTATOR)
        {
            sender.sendMessage(ChatColor.GREEN + "You will spectate the game!");
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "You will take part in the game!");
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        return null;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }

    private final GameStateTracker m_gameStateTracker;
}
