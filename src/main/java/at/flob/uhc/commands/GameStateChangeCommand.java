// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControl;
import org.bukkit.command.CommandSender;

import java.util.List;


/**
 * @author Florian Graf
 */
public class GameStateChangeCommand
    implements Command
{
    public GameStateChangeCommand(GameStateTracker gameStateTracker, GameState targetState)
    {
        m_gameStateTracker = gameStateTracker;
        m_targetState = targetState;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        m_gameStateTracker.tryChangeState(m_targetState);
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        return null;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_OPERATOR;
    }

    private final GameStateTracker m_gameStateTracker;
    private final GameState m_targetState;
}
