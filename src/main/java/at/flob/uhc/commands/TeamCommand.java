// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.ServerControl;
import at.flob.uhc.TeamManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TeamCommand
    implements Command
{
    private static final String SUB_COMMAND_ADD = "ADD";
    private static final String SUB_COMMAND_COLOR = "COLOR";

    private static final Map<String, ChatColor> CHAT_COLOR_NAMES = new HashMap<>();

    static
    {
        CHAT_COLOR_NAMES.put("AQUA", ChatColor.AQUA);
        CHAT_COLOR_NAMES.put("BLACK", ChatColor.BLACK);
        CHAT_COLOR_NAMES.put("BLUE", ChatColor.BLUE);
        CHAT_COLOR_NAMES.put("DARK_AQUA", ChatColor.DARK_AQUA);
        CHAT_COLOR_NAMES.put("DARK_BLUE", ChatColor.DARK_BLUE);
        CHAT_COLOR_NAMES.put("DARK_GRAY", ChatColor.DARK_GRAY);
        CHAT_COLOR_NAMES.put("DARK_GREEN", ChatColor.DARK_GREEN);
        CHAT_COLOR_NAMES.put("DARK_PURPLE", ChatColor.DARK_PURPLE);
        CHAT_COLOR_NAMES.put("DARK_RED", ChatColor.DARK_RED);
        CHAT_COLOR_NAMES.put("GOLD", ChatColor.GOLD);
        CHAT_COLOR_NAMES.put("GRAY", ChatColor.GRAY);
        CHAT_COLOR_NAMES.put("GREEN", ChatColor.GREEN);
        CHAT_COLOR_NAMES.put("LIGHT_PURPLE", ChatColor.LIGHT_PURPLE);
        CHAT_COLOR_NAMES.put("RED", ChatColor.RED);
        CHAT_COLOR_NAMES.put("YELLOW", ChatColor.YELLOW);
    }

    public TeamCommand(ServerControl serverControl, TeamManager teamManager, GameStateTracker gameStateTracker)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        if(teamManager == null)
        {
            throw new IllegalArgumentException("The team manager cannot be null!");
        }

        if(gameStateTracker == null)
        {
            throw new IllegalArgumentException("The game state tracker cannot be null!");
        }

        m_serverControl = serverControl;
        m_teamManager = teamManager;
        m_gameStateTracker = gameStateTracker;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(arguments.getLength() < 3)
        {
            sender.sendMessage(ChatColor.RED + "Not enough arguments provided!");
            return;
        }

        if(m_gameStateTracker.getCurrentState() != GameState.CREATED
            && !sender.isOp() && !sender.hasPermission(Command.PERMISSION_OPERATOR))
        {
            sender.sendMessage(ChatColor.RED + "You can only issue team command during game preparations!");
            return;
        }

        String teamName = arguments.getArgument(0).trim();

        if(sender.isOp() || sender.hasPermission(Command.PERMISSION_OPERATOR))
        {
            if(!m_teamManager.getTeamNames().contains(teamName))
            {
                sender.sendMessage(ChatColor.RED + "Unknown team: " + teamName);
                return;
            }
        }
        else if(sender instanceof Player)
        {
            Player player = (Player)sender;
            Team team = m_teamManager.getTeam(player);
            if(team == null || !team.getName().equals(teamName))
            {
                sender.sendMessage("You're not part of this team: " + teamName);
                return;
            }
        }
        else
        {
            sender.sendMessage(ChatColor.RED +
                "You are not part of any team and don't have permission to change other team colors!");
            return;
        }

        String subCommand = arguments.getArgument(1);
        switch(subCommand.toUpperCase())
        {
        case SUB_COMMAND_ADD:
            if(!sender.hasPermission(Command.PERMISSION_OPERATOR))
            {
                sender.sendMessage(ChatColor.RED + "You don't have permission to use that command!");
                return;
            }

            addPlayerToTeam(sender, teamName, arguments.getArgument(2));
            break;
        case SUB_COMMAND_COLOR:
            updateTeamColor(sender, teamName, arguments.getArgument(2));
            break;
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> results = new ArrayList<>();

        if(arguments.getLength() == 1)
        {
            String teamNamePart = arguments.getArgument(0);

            if(sender.isOp() || sender.hasPermission(PERMISSION_OPERATOR))
            {
                for(String teamName : m_teamManager.getTeamNames())
                {
                    if(teamName.startsWith(teamNamePart))
                    {
                        results.add(teamName);
                    }
                }
            }
            else if(sender instanceof Player)
            {
                Team team = m_teamManager.getTeam((Player)sender);
                if(team != null && team.getName().startsWith(teamNamePart))
                {
                    results.add(team.getName());
                }
            }
        }
        else if(arguments.getLength() == 2)
        {
            String subCommand = arguments.getArgument(1).toUpperCase();
            if(SUB_COMMAND_COLOR.startsWith(subCommand))
            {
                results.add(SUB_COMMAND_COLOR.toLowerCase());
            }

            if(sender.hasPermission(Command.PERMISSION_OPERATOR) && SUB_COMMAND_ADD.startsWith(subCommand))
            {
                results.add(SUB_COMMAND_ADD.toLowerCase());
            }
        }
        else if(arguments.getLength() == 3)
        {
            switch(arguments.getArgument(1).toUpperCase())
            {
            case SUB_COMMAND_ADD:
                if(sender.hasPermission(Command.PERMISSION_OPERATOR) || sender.isOp())
                {
                    String playerPart = arguments.getArgument(2);
                    for(Player player : m_serverControl.getOnlinePlayers())
                    {
                        if(player.getName().startsWith(playerPart))
                        {
                            results.add(player.getName());
                        }
                    }
                }
                break;
            case SUB_COMMAND_COLOR:
                for(ChatColor availableColor : m_teamManager.getAvailableColors())
                {
                    String colorPart = arguments.getArgument(2).toUpperCase();
                    for(Map.Entry<String, ChatColor> entry : CHAT_COLOR_NAMES.entrySet())
                    {
                        if(entry.getValue() == availableColor && entry.getKey().startsWith(colorPart))
                        {
                            results.add(entry.getKey().toLowerCase());
                        }
                    }
                }
                break;
            }
        }

        return results;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }

    private void updateTeamColor(CommandSender sender, String teamName, String possibleColor)
    {
        ChatColor color = CHAT_COLOR_NAMES.get(possibleColor.toUpperCase());
        if(color == null)
        {
            sender.sendMessage(ChatColor.RED + "Unknown color: " + possibleColor);
            return;
        }

        for(ChatColor availableColor : m_teamManager.getAvailableColors())
        {
            if(availableColor == color)
            {
                if(m_teamManager.updateTeamColor(teamName, color))
                {
                    sender.sendMessage("The team color has been updated successfully to " + color + possibleColor);
                }
                else
                {
                    sender.sendMessage(ChatColor.RED + "Couldn't change the team color. " +
                        "Try again or contact your support!");
                }

                return;
            }
        }

        sender.sendMessage(ChatColor.RED + "This color is already in use by another team!");
    }

    private void addPlayerToTeam(CommandSender sender, String teamName, String possiblePlayerName)
    {
        Player playerToAdd = null;
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            if(player.getName().equalsIgnoreCase(possiblePlayerName))
            {
                playerToAdd = player;
                break;
            }
        }

        if(playerToAdd == null)
        {
            sender.sendMessage(ChatColor.RED + "Unknown player: " + possiblePlayerName);
            return;
        }

        m_teamManager.joinTeam(playerToAdd, teamName);
        sender.sendMessage("Added " + possiblePlayerName + " to the team " + teamName);
    }

    private final ServerControl m_serverControl;
    private final TeamManager m_teamManager;
    private final GameStateTracker m_gameStateTracker;
}
