// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.logging.LoggerFactory;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * @author Florian Graf
 */
public class ConfigCommand
    implements Command
{
    private final static Logger s_logger = LoggerFactory.getLogger(ConfigCommand.class);

    public ConfigCommand(GameStateTracker gameStateTracker, PluginConfiguration configuration)
    {
        if(gameStateTracker == null)
        {
            throw new IllegalArgumentException("The game state tracker cannot be null!");
        }

        if(configuration == null)
        {
            throw new IllegalArgumentException("The configuration cannot be null!");
        }

        m_gameStateTracker = gameStateTracker;
        m_configuration = configuration;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(arguments.getLength() < 1)
        {
            sender.sendMessage(ChatColor.RED + "Invalid command, expected get, set or list and got nothing!");
            return;
        }

        String getOrSet = arguments.getArgument(0);
        if(getOrSet.equalsIgnoreCase("get"))
        {
            getConfig(sender, arguments);
        }
        else if(getOrSet.equalsIgnoreCase("set"))
        {
            setConfig(sender, arguments);
        }
        else if(getOrSet.equalsIgnoreCase("list"))
        {
            listConfig(sender, arguments);
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "Invalid command, expected get, set or list and got " + getOrSet + "!");
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> result = new ArrayList<>();

        if(arguments.getLength() == 1)
        {
            if("GET".startsWith(arguments.getArgument(0).toUpperCase()))
            {
                result.add("get");
            }

            if("SET".startsWith(arguments.getArgument(0).toUpperCase()))
            {
                result.add("set");
            }

            if("LIST".startsWith(arguments.getArgument(0).toUpperCase()))
            {
                result.add("list");
            }
        }
        else if(arguments.getLength() == 2)
        {
            String firstArgument = arguments.getArgument(0);
            if(firstArgument.equalsIgnoreCase("get") || firstArgument.equalsIgnoreCase("set"))
            {
                String secondArgument = arguments.getArgument(1);
                for(String configKey : m_configuration.getPluginEntries().keySet())
                {
                    if(configKey.startsWith(secondArgument))
                    {
                        result.add(configKey);
                    }
                }
            }
        }
        else if(arguments.getLength() == 3)
        {
            String firstArgument = arguments.getArgument(0);
            if(firstArgument.equalsIgnoreCase("set"))
            {
                String configKey = arguments.getArgument(1);
                if(m_configuration.getPluginEntries().containsKey(configKey))
                {
                    result.add("VALUE");
                }
            }
        }

        return result;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_OPERATOR;
    }

    private void getConfig(CommandSender sender, CommandArguments arguments)
    {
        if(arguments.getLength() < 2)
        {
            sender.sendMessage(ChatColor.RED + "Invalid command, expected: /flobuhc config get <key>");
            return;
        }

        String key = arguments.getArgument(1);
        PluginConfiguration.Entry entry = m_configuration.getPluginEntries().get(key);

        if(entry == null)
        {
            sender.sendMessage(ChatColor.RED + "Unknown configuration key: " + key);
        }
        else
        {
            sender.sendMessage(key + ": " + entry.getValue());
        }
    }

    private void setConfig(CommandSender sender, CommandArguments arguments)
    {
        GameState currentState = m_gameStateTracker.getCurrentState();
        if(currentState != GameState.STANDBY && currentState != GameState.CREATED)
        {
            sender.sendMessage(ChatColor.RED + "Cannot change configuration while the game is running!");
        }

        if(arguments.getLength() < 3)
        {
            sender.sendMessage(ChatColor.RED + "Invalid command, expected: /flobuhc config set <key> <value>");
            return;
        }

        String key = arguments.getArgument(1);
        PluginConfiguration.Entry entry = m_configuration.getPluginEntries().get(key);

        if(entry == null)
        {
            sender.sendMessage(ChatColor.RED + "Unknown configuration key: " + key);
        }
        else
        {
            try
            {
                entry.setValue(arguments.getArgument(2));
                String newValue = entry.getValue();

                s_logger.info(() -> sender.getName() + " changed configuration " + key + " to " + newValue);
                sender.sendMessage(ChatColor.GREEN + key + ": " + newValue);
            }
            catch(NumberFormatException exception)
            {
                sender.sendMessage(ChatColor.RED + "Failed to set new value: " + exception.getMessage());
            }
        }
    }

    private void listConfig(CommandSender sender, CommandArguments arguments)
    {
        Map<String, PluginConfiguration.Entry> entries = m_configuration.getPluginEntries();

        ArrayList<String> keys = new ArrayList<>(entries.keySet());
        keys.sort(String::compareToIgnoreCase);

        for(String key : keys)
        {
            sender.sendMessage(key + ": " + entries.get(key).getValue());
        }
    }

    private final GameStateTracker m_gameStateTracker;
    private final PluginConfiguration m_configuration;
}
