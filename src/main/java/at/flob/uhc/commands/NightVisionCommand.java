// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;


/**
 * @author Florian Graf
 */
public class NightVisionCommand
    implements Command
{

    private static final PotionEffect NIGHT_VISION = new PotionEffect(
        PotionEffectType.NIGHT_VISION,
        1000000,
        0,
        false,
        false);

    public static void giveNightVision(Player player)
    {
        player.addPotionEffect(NIGHT_VISION);
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "You have to be a player to use this command!");
            return;
        }

        Player player = (Player)sender;

        if(player.getGameMode() != GameMode.SPECTATOR)
        {
            sender.sendMessage(ChatColor.RED + "You have to be a spectator to use this command!");
            return;
        }

        if(player.hasPotionEffect(PotionEffectType.NIGHT_VISION))
        {
            player.removePotionEffect(PotionEffectType.NIGHT_VISION);
        }
        else
        {
            giveNightVision(player);
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        return null;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }
}
