// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import at.flob.uhc.Utilities;
import at.flob.uhc.configuration.AppliedEffectsConfiguration;
import at.flob.uhc.configuration.TrainingConfiguration;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class TrainingCommand
    implements Command
{
    private static final String SUB_COMMAND_START = "START";
    private static final String SUB_COMMAND_EXIT = "EXIT";
    private static final String SUB_COMMAND_HELP = "HELP";

    public TrainingCommand(
        ServerControl serverControl,
        GameStateTracker gameStateTracker,
        AppliedEffectsConfiguration appliedEffectsConfiguration)
    {
        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        if(gameStateTracker == null)
        {
            throw new IllegalArgumentException("The game state tracker cannot be null!");
        }

        if(appliedEffectsConfiguration == null)
        {
            throw new IllegalArgumentException("The applied effects configuration cannot be null!");
        }

        m_random = new Random();
        m_serverControl = serverControl;
        m_gameStateTracker = gameStateTracker;
        m_appliedEffectsConfiguration = appliedEffectsConfiguration;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        if(m_gameStateTracker.getCurrentState() != GameState.CREATED)
        {
            sender.sendMessage(ChatColor.RED + "Training is only available during game preparations!");
            return;
        }

        if(TrainingConfiguration.isDeactivated())
        {
            sender.sendMessage(ChatColor.RED + "Training is disabled on this server!");
            return;
        }

        if(arguments.getLength() < 1)
        {
            sender.sendMessage(ChatColor.RED + "Missing start or exit argument!");
            return;
        }

        if(!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "Only players can use this command!");
            return;
        }

        Player player = (Player)sender;

        String subCommand = arguments.getArgument(0);
        switch(subCommand.toUpperCase())
        {
        case SUB_COMMAND_START:
            startTraining(player);
            break;
        case SUB_COMMAND_EXIT:
            exitTraining(player);
            break;
        case SUB_COMMAND_HELP:
            displayHelp(player);
            break;
        }
    }

    @Override
    public List<String> getTabCompleteList(CommandSender sender, CommandArguments arguments)
    {
        List<String> results = new ArrayList<>();
        String subCommand = arguments.getArgument(0).toUpperCase();

        if(SUB_COMMAND_START.startsWith(subCommand))
        {
            results.add(SUB_COMMAND_START.toLowerCase());
        }

        if(SUB_COMMAND_EXIT.startsWith(subCommand))
        {
            results.add(SUB_COMMAND_EXIT.toLowerCase());
        }

        if(SUB_COMMAND_HELP.startsWith(subCommand))
        {
            results.add(SUB_COMMAND_HELP.toLowerCase());
        }

        return results;
    }

    @Override
    public String getRequiredPermission()
    {
        return Command.PERMISSION_PLAYER;
    }

    private void startTraining(Player player)
    {
        TrainingConfiguration.setTrainee(player, true);
        player.getInventory().clear();
        player.setExp(0);

        Location trainingStart = getRandomTrainingStartLocation();
        player.teleport(trainingStart);
        player.setBedSpawnLocation(trainingStart, true);
        Utilities.clearPotionEffects(player);

        TrainingConfiguration.equipPlayer(player);

        displayExitHelp(player);
    }

    private void exitTraining(Player player)
    {
        TrainingConfiguration.setTrainee(player, false);
        player.getInventory().clear();
        player.setExp(0);

        String worldName = getConfiguration().getString(PluginConfiguration.GAME_AREA_START_WORLD, "world");
        World world = m_serverControl.getWorld(worldName);

        double centerX = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_START_CENTER_X, 0);
        double centerY = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_START_CENTER_Y, Double.NaN);
        double centerZ = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_START_CENTER_Z, 0);

        if(Double.isNaN(centerY))
        {
            centerY = Utilities.findSafeYForSpawn(world, Location.locToBlock(centerX), Location.locToBlock(centerZ));
        }

        Location location = new Location(world, centerX, centerY, centerZ);
        player.teleport(location);
        player.setBedSpawnLocation(location);

        Utilities.clearPotionEffects(player);
        m_appliedEffectsConfiguration.applyPotionEffects(player);
    }

    private void displayHelp(Player player)
    {
        player.sendMessage("With /flobuhc training start you get teleported to a training ground to improve your " +
            "fighting skills!");

        displayExitHelp(player);
    }

    private void displayExitHelp(Player player)
    {
        player.sendMessage("If you think that you're good enough, you can exit the training ground with " +
            "/flobuhc training exit");
    }

    private Location getRandomTrainingStartLocation()
    {
        String worldName = getConfiguration().getString(PluginConfiguration.GAME_AREA_TRAINING_WORLD, "world");
        World world = m_serverControl.getWorld(worldName);

        double centerX = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_CENTER_X, 0);
        double centerZ = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_CENTER_Z, 0);
        double radius = getConfiguration().getDouble(PluginConfiguration.GAME_AREA_TRAINING_RADIUS, 100);

        double spawnX = centerX;
        double spawnZ = centerZ;
        int safeSpawnY = -1;

        while(safeSpawnY < 0)
        {
            spawnX = centerX + m_random.nextDouble() * radius * (m_random.nextBoolean() ? -1 : 1);
            spawnZ = centerZ + m_random.nextDouble() * radius * (m_random.nextBoolean() ? -1 : 1);

            safeSpawnY = Utilities.findSafeYForSpawn(
                world,
                Location.locToBlock(spawnX),
                Location.locToBlock(spawnZ));
        }

        return new Location(world, spawnX, safeSpawnY, spawnZ);
    }

    private PluginConfiguration getConfiguration()
    {
        return m_serverControl.getConfiguration();
    }

    private final Random m_random;
    private final ServerControl m_serverControl;
    private final GameStateTracker m_gameStateTracker;
    private final AppliedEffectsConfiguration m_appliedEffectsConfiguration;
}
