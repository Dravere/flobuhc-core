// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


/**
 * Encapsulates the command arguments from a string array with optional offset.
 *
 * @author Florian Graf
 */
public class CommandArguments
{
    /**
     * Command arguments with a length of 0.
     */
    public static final CommandArguments NONE = new CommandArguments(new String[0], 0, 0);

    /**
     * Creates the command arguments from an array of string with a given offset.
     *
     * @param arguments The command arguments array to be used.
     * @param offset The offset from which the command arguments start.
     */
    public CommandArguments(String[] arguments, int offset)
    {
        this(arguments, offset, arguments.length - offset);
    }

    /**
     * Creates the command arguments from an array of strings with offset and length.
     *
     * @param arguments The command arguments array to be used.
     * @param offset The offset from which the command arguments start.
     * @param length The length of the command arguments.
     *
     * @throws IllegalArgumentException If arguments is null or there are not enough arguments in the array.
     */
    public CommandArguments(String[] arguments, int offset, int length)
    {
        if(arguments == null)
        {
            throw new IllegalArgumentException("The arguments can't be null!");
        }

        if((offset + length) > arguments.length)
        {
            throw new IllegalArgumentException("The arguments array is shorter than the given offset plus length!");
        }

        m_arguments = arguments;
        m_offset = offset;
        m_length = length;
    }

    public int getLength()
    {
        return m_length;
    }

    public String getArgument(int index)
    {
        return m_arguments[m_offset + index];
    }

    private final String[] m_arguments;
    private final int m_offset;
    private final int m_length;
}
