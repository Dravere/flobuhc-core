// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.commands;


import at.flob.uhc.GameState;
import at.flob.uhc.GameStateTracker;
import at.flob.uhc.Melody;
import at.flob.uhc.MelodyPlay;
import at.flob.uhc.PluginScheduler;
import at.flob.uhc.ServerControl;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;


/**
 * @author Florian Graf
 */
public class DelayedGameStateChangeCommand
    extends GameStateChangeCommand
{
    public DelayedGameStateChangeCommand(
        ServerControl serverControl,
        GameStateTracker gameStateTracker,
        GameState targetState,
        long delayMilliseconds)
    {
        super(gameStateTracker, targetState);

        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
        m_delayMilliseconds = delayMilliseconds;
    }

    @Override
    public void execute(CommandSender sender, CommandArguments arguments)
    {
        Melody.GAME_START.play(m_serverControl.getScheduler(), m_serverControl.getOnlinePlayers(), Instrument.PIANO);

        if(m_delayMilliseconds > 0)
        {
            sender.sendMessage(ChatColor.GOLD + "The game state will change in " + m_delayMilliseconds + "ms!");

            m_serverControl.getScheduler().runTaskDelayed(
                () -> DelayedGameStateChangeCommand.super.execute(sender, arguments),
                PluginScheduler.ms2t(m_delayMilliseconds));
        }
        else
        {
            super.execute(sender, arguments);
        }
    }

    private final ServerControl m_serverControl;
    private final long m_delayMilliseconds;
}
