// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;


public class Utilities
{
    private static final String NATURAL_REGENERATION_RULE = "naturalRegeneration";
    private static final String RULE_VALUE_TRUE = "true";
    private static final String RULE_VALUE_FALSE = "false";

    public static Location findBlockWithDirectSkyView(Location location)
    {
        Location found = location.clone();
        boolean foundIsGood = found.getBlock().isEmpty();

        World world = location.getWorld();

        for(; location.getBlockY() < world.getMaxHeight(); location.add(0, 1, 0))
        {
            if(!location.getBlock().isEmpty())
            {
                foundIsGood = false;
            }
            else
            {
                if(!foundIsGood)
                {
                    found = location.clone();
                }

                foundIsGood = true;
            }
        }

        return found;
    }

    public static int findSafeYForSpawn(World world, int x, int z)
    {
        for(int y = 255; y > 0; --y)
        {
            Block block = world.getBlockAt(x, y, z);
            Material material = block.getType();

            if(material != Material.AIR)
            {
                if(!block.isLiquid() && material != Material.FIRE)
                {
                    return y + 1;
                }

                return -1;
            }
        }

        return -1;
    }

    public static void clearPotionEffects(Player player)
    {
        for(PotionEffect effect : player.getActivePotionEffects())
        {
            player.removePotionEffect(effect.getType());
        }
    }

    public static void setHealthRegeneration(World world, boolean enabled)
    {
        world.setGameRuleValue(NATURAL_REGENERATION_RULE, enabled ? RULE_VALUE_TRUE : RULE_VALUE_FALSE);
    }

    public static void renameItemStack(ItemStack stack, String displayName)
    {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(displayName);
        stack.setItemMeta(meta);
    }
}
