// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import org.bukkit.Color;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationOptions;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author Florian Graf
 */
public class PluginConfiguration
    implements Configuration
{
    public interface Entry
    {
        String getValue();
        void setValue(String value);
    }

    public static final String GAME_SPREAD_KEEP_TEAMS = "game.spread.keepTeams";
    public static final String GAME_SPREAD_MIN_DISTANCE = "game.spread.minDistance";

    public static final String GAME_AREA_START_WORLD = "game.area.start.world";
    public static final String GAME_AREA_START_CENTER_X = "game.area.start.centerX";
    public static final String GAME_AREA_START_CENTER_Y = "game.area.start.centerY";
    public static final String GAME_AREA_START_CENTER_Z = "game.area.start.centerZ";
    public static final String GAME_AREA_START_RADIUS = "game.area.start.radius";
    public static final String GAME_AREA_START_CAGEY = "game.area.start.cageY";

    public static final String GAME_AREA_TRAINING_ENABLED = "game.area.training.enabled";
    public static final String GAME_AREA_TRAINING_WORLD = "game.area.training.world";
    public static final String GAME_AREA_TRAINING_CENTER_X = "game.area.training.centerX";
    public static final String GAME_AREA_TRAINING_CENTER_Z = "game.area.training.centerZ";
    public static final String GAME_AREA_TRAINING_RADIUS = "game.area.training.radius";

    public static final String GAME_AREA_PLAY_WORLD = "game.area.play.world";
    public static final String GAME_AREA_PLAY_CENTER_X = "game.area.play.centerX";
    public static final String GAME_AREA_PLAY_CENTER_Y = "game.area.play.centerY";
    public static final String GAME_AREA_PLAY_CENTER_Z = "game.area.play.centerZ";
    public static final String GAME_AREA_PLAY_RADIUS = "game.area.play.radius";
    public static final String GAME_AREA_PLAY_CAGEY = "game.area.play.cageY";

    public static final String GAME_AREA_SHRINK_ENABLED = "game.area.shrink.enabled";
    public static final String GAME_AREA_SHRINK_RADIUS = "game.area.shrink.radius";
    public static final String GAME_AREA_SHRINK_TIME = "game.area.shrink.time";
    public static final String GAME_AREA_SHRINK_HOLD_OFF = "game.area.shrink.holdOff";
    
    public static final String GAME_TIMER_EPISODE = "game.timer.episode";

    public static final String GAME_COMBAT_HOLD_OFF = "game.combat.holdOff";
    public static final String GAME_COMBAT_PROTECTION = "game.combat.protection";

    public static final String GAME_DAYLIGHT_START_ENABLED = "game.daylightCycle.start.enabled";
    public static final String GAME_DAYLIGHT_START_TIME = "game.daylightCycle.start.time";
    public static final String GAME_DAYLIGHT_PLAY_ENABLED = "game.daylightCycle.play.enabled";
    public static final String GAME_DAYLIGHT_PLAY_TIME = "game.daylightCycle.play.time";
    public static final String GAME_DAYLIGHT_FINISH_ENABLED = "game.daylightCycle.finish.enabled";
    public static final String GAME_DAYLIGHT_FINISH_TIME = "game.daylightCycle.finish.time";

    public static final String GAME_SPECTATING_VIEW_PLAYER_INVENTORY = "game.spectating.viewPlayerInventory";

    public static final String GAME_FIREWORKS_ENABLED = "game.fireworks.enabled";
    public static final String GAME_FIREWORKS_POWER = "game.fireworks.power";
    public static final String GAME_FIREWORKS_HOLD_OFF = "game.fireworks.holdOff";
    public static final String GAME_FIREWORKS_EVERY = "game.fireworks.every";

    public static final String GAME_TEAMS_PREFIX = "game.teams.prefix";
    public static final String GAME_TEAMS_SUFFIX = "game.teams.suffix";

    public static final String COMPATIBILITY_MULTIVERSE = "compatibility.multiVerse";

    public PluginConfiguration(Plugin plugin)
    {
        if(plugin == null)
        {
            throw new IllegalArgumentException("The plugin cannot be null!");
        }

        m_plugin = plugin;
        m_configEntries = new HashMap<>();
        m_configEntries.put(GAME_SPREAD_KEEP_TEAMS, new BooleanEntry(this, GAME_SPREAD_KEEP_TEAMS));
        m_configEntries.put(GAME_SPREAD_MIN_DISTANCE, new DoubleEntry(this, GAME_SPREAD_MIN_DISTANCE));
        m_configEntries.put(GAME_AREA_START_WORLD, new StringEntry(this, GAME_AREA_START_WORLD));
        m_configEntries.put(GAME_AREA_START_CENTER_X, new DoubleEntry(this, GAME_AREA_START_CENTER_X));
        m_configEntries.put(GAME_AREA_START_CENTER_Y, new DoubleEntry(this, GAME_AREA_START_CENTER_Y));
        m_configEntries.put(GAME_AREA_START_CENTER_Z, new DoubleEntry(this, GAME_AREA_START_CENTER_Z));
        m_configEntries.put(GAME_AREA_START_RADIUS, new DoubleEntry(this, GAME_AREA_START_RADIUS));
        m_configEntries.put(GAME_AREA_START_CAGEY, new DoubleEntry(this, GAME_AREA_START_CAGEY));
        m_configEntries.put(GAME_AREA_TRAINING_ENABLED, new BooleanEntry(this, GAME_AREA_TRAINING_ENABLED));
        m_configEntries.put(GAME_AREA_TRAINING_WORLD, new StringEntry(this, GAME_AREA_TRAINING_WORLD));
        m_configEntries.put(GAME_AREA_TRAINING_CENTER_X, new DoubleEntry(this, GAME_AREA_TRAINING_CENTER_X));
        m_configEntries.put(GAME_AREA_TRAINING_CENTER_Z, new DoubleEntry(this, GAME_AREA_TRAINING_CENTER_Z));
        m_configEntries.put(GAME_AREA_TRAINING_RADIUS, new DoubleEntry(this, GAME_AREA_TRAINING_RADIUS));
        m_configEntries.put(GAME_AREA_PLAY_WORLD, new StringEntry(this, GAME_AREA_PLAY_WORLD));
        m_configEntries.put(GAME_AREA_PLAY_CENTER_X, new DoubleEntry(this, GAME_AREA_PLAY_CENTER_X));
        m_configEntries.put(GAME_AREA_PLAY_CENTER_Y, new DoubleEntry(this, GAME_AREA_PLAY_CENTER_Y));
        m_configEntries.put(GAME_AREA_PLAY_CENTER_Z, new DoubleEntry(this, GAME_AREA_PLAY_CENTER_Z));
        m_configEntries.put(GAME_AREA_PLAY_RADIUS, new DoubleEntry(this, GAME_AREA_PLAY_RADIUS));
        m_configEntries.put(GAME_AREA_PLAY_CAGEY, new DoubleEntry(this, GAME_AREA_PLAY_CAGEY));
        m_configEntries.put(GAME_AREA_SHRINK_ENABLED, new BooleanEntry(this, GAME_AREA_SHRINK_ENABLED));
        m_configEntries.put(GAME_AREA_SHRINK_RADIUS, new DoubleEntry(this, GAME_AREA_SHRINK_RADIUS));
        m_configEntries.put(GAME_AREA_SHRINK_TIME, new LongEntry(this, GAME_AREA_SHRINK_TIME));
        m_configEntries.put(GAME_AREA_SHRINK_HOLD_OFF, new LongEntry(this, GAME_AREA_SHRINK_HOLD_OFF));
        m_configEntries.put(GAME_TIMER_EPISODE, new LongEntry(this, GAME_TIMER_EPISODE));
        m_configEntries.put(GAME_COMBAT_HOLD_OFF, new LongEntry(this, GAME_COMBAT_HOLD_OFF));
        m_configEntries.put(GAME_COMBAT_PROTECTION, new LongEntry(this, GAME_COMBAT_PROTECTION));
        m_configEntries.put(GAME_DAYLIGHT_START_ENABLED, new BooleanEntry(this, GAME_DAYLIGHT_START_ENABLED));
        m_configEntries.put(GAME_DAYLIGHT_START_TIME, new LongEntry(this, GAME_DAYLIGHT_START_TIME));
        m_configEntries.put(GAME_DAYLIGHT_PLAY_ENABLED, new BooleanEntry(this, GAME_DAYLIGHT_PLAY_ENABLED));
        m_configEntries.put(GAME_DAYLIGHT_PLAY_TIME, new LongEntry(this, GAME_DAYLIGHT_PLAY_TIME));
        m_configEntries.put(GAME_DAYLIGHT_FINISH_ENABLED, new BooleanEntry(this, GAME_DAYLIGHT_FINISH_ENABLED));
        m_configEntries.put(GAME_DAYLIGHT_FINISH_TIME, new LongEntry(this, GAME_DAYLIGHT_FINISH_TIME));
        m_configEntries.put(GAME_SPECTATING_VIEW_PLAYER_INVENTORY, new BooleanEntry(this, GAME_SPECTATING_VIEW_PLAYER_INVENTORY));
        m_configEntries.put(GAME_FIREWORKS_ENABLED, new BooleanEntry(this, GAME_FIREWORKS_ENABLED));
        m_configEntries.put(GAME_FIREWORKS_POWER, new LongEntry(this, GAME_FIREWORKS_POWER));
        m_configEntries.put(GAME_FIREWORKS_HOLD_OFF, new LongEntry(this, GAME_FIREWORKS_HOLD_OFF));
        m_configEntries.put(GAME_FIREWORKS_EVERY, new LongEntry(this, GAME_FIREWORKS_EVERY));
        m_configEntries.put(GAME_TEAMS_PREFIX, new StringEntry(this, GAME_TEAMS_PREFIX));
        m_configEntries.put(GAME_TEAMS_SUFFIX, new StringEntry(this, GAME_TEAMS_SUFFIX));
        m_configEntries.put(COMPATIBILITY_MULTIVERSE, new BooleanEntry(this, COMPATIBILITY_MULTIVERSE));
    }

    public void save()
    {
        m_plugin.saveConfig();
    }

    public void reload()
    {
        m_plugin.reloadConfig();
    }

    public void saveDefault()
    {
        m_plugin.saveDefaultConfig();
    }

    public Map<String, Entry> getPluginEntries()
    {
        return new HashMap<>(m_configEntries);
    }

    @Override
    public Set<String> getKeys(boolean deep)
    {
        return m_plugin.getConfig().getKeys(deep);
    }

    @Override
    public Map<String, Object> getValues(boolean deep)
    {
        return m_plugin.getConfig().getValues(deep);
    }

    @Override
    public boolean contains(String path)
    {
        return m_plugin.getConfig().contains(path);
    }

    @Override
    public boolean contains(String path, boolean ignoreDefault)
    {
        return m_plugin.getConfig().contains(path, ignoreDefault);
    }

    @Override
    public boolean isSet(String path)
    {
        return m_plugin.getConfig().isSet(path);
    }

    @Override
    public String getCurrentPath()
    {
        return m_plugin.getConfig().getCurrentPath();
    }

    @Override
    public String getName()
    {
        return m_plugin.getConfig().getName();
    }

    @Override
    public Configuration getRoot()
    {
        return m_plugin.getConfig().getRoot();
    }

    @Override
    public ConfigurationSection getParent()
    {
        return m_plugin.getConfig().getParent();
    }

    @Override
    public Object get(String path)
    {
        return m_plugin.getConfig().get(path);
    }

    @Override
    public Object get(String path, Object def)
    {
        return m_plugin.getConfig().get(path, def);
    }

    @Override
    public void set(String path, Object value)
    {
        m_plugin.getConfig().set(path, value);
    }

    @Override
    public ConfigurationSection createSection(String path)
    {
        return m_plugin.getConfig().createSection(path);
    }

    @Override
    public ConfigurationSection createSection(String path, Map<?, ?> map)
    {
        return m_plugin.getConfig().createSection(path, map);
    }

    @Override
    public String getString(String path)
    {
        return m_plugin.getConfig().getString(path);
    }

    @Override
    public String getString(String path, String def)
    {
        return m_plugin.getConfig().getString(path, def);
    }

    @Override
    public boolean isString(String path)
    {
        return m_plugin.getConfig().isString(path);
    }

    @Override
    public int getInt(String path)
    {
        return m_plugin.getConfig().getInt(path);
    }

    @Override
    public int getInt(String path, int def)
    {
        return m_plugin.getConfig().getInt(path, def);
    }

    @Override
    public boolean isInt(String path)
    {
        return m_plugin.getConfig().isInt(path);
    }

    @Override
    public boolean getBoolean(String path)
    {
        return m_plugin.getConfig().getBoolean(path);
    }

    @Override
    public boolean getBoolean(String path, boolean def)
    {
        return m_plugin.getConfig().getBoolean(path, def);
    }

    @Override
    public boolean isBoolean(String path)
    {
        return m_plugin.getConfig().isBoolean(path);
    }

    @Override
    public double getDouble(String path)
    {
        return m_plugin.getConfig().getDouble(path);
    }

    @Override
    public double getDouble(String path, double def)
    {
        return m_plugin.getConfig().getDouble(path, def);
    }

    @Override
    public boolean isDouble(String path)
    {
        return m_plugin.getConfig().isDouble(path);
    }

    @Override
    public long getLong(String path)
    {
        return m_plugin.getConfig().getLong(path);
    }

    @Override
    public long getLong(String path, long def)
    {
        return m_plugin.getConfig().getLong(path, def);
    }

    @Override
    public boolean isLong(String path)
    {
        return m_plugin.getConfig().isLong(path);
    }

    @Override
    public List<?> getList(String path)
    {
        return m_plugin.getConfig().getList(path);
    }

    @Override
    public List<?> getList(String path, List<?> def)
    {
        return m_plugin.getConfig().getList(path, def);
    }

    @Override
    public boolean isList(String path)
    {
        return m_plugin.getConfig().isList(path);
    }

    @Override
    public List<String> getStringList(String path)
    {
        return m_plugin.getConfig().getStringList(path);
    }

    @Override
    public List<Integer> getIntegerList(String path)
    {
        return m_plugin.getConfig().getIntegerList(path);
    }

    @Override
    public List<Boolean> getBooleanList(String path)
    {
        return m_plugin.getConfig().getBooleanList(path);
    }

    @Override
    public List<Double> getDoubleList(String path)
    {
        return m_plugin.getConfig().getDoubleList(path);
    }

    @Override
    public List<Float> getFloatList(String path)
    {
        return m_plugin.getConfig().getFloatList(path);
    }

    @Override
    public List<Long> getLongList(String path)
    {
        return m_plugin.getConfig().getLongList(path);
    }

    @Override
    public List<Byte> getByteList(String path)
    {
        return m_plugin.getConfig().getByteList(path);
    }

    @Override
    public List<Character> getCharacterList(String path)
    {
        return m_plugin.getConfig().getCharacterList(path);
    }

    @Override
    public List<Short> getShortList(String path)
    {
        return m_plugin.getConfig().getShortList(path);
    }

    @Override
    public List<Map<?, ?>> getMapList(String path)
    {
        return m_plugin.getConfig().getMapList(path);
    }

    @Override
    public <T extends ConfigurationSerializable> T getSerializable(
        String path, Class<T> clazz)
    {
        return m_plugin.getConfig().getSerializable(path, clazz);
    }

    @Override
    public <T extends ConfigurationSerializable> T getSerializable(String path, Class<T> clazz, T def)
    {
        return m_plugin.getConfig().getSerializable(path, clazz, def);
    }

    @Override
    public Vector getVector(String path)
    {
        return m_plugin.getConfig().getVector(path);
    }

    @Override
    public Vector getVector(String path, Vector def)
    {
        return m_plugin.getConfig().getVector(path, def);
    }

    @Override
    public boolean isVector(String path)
    {
        return m_plugin.getConfig().isVector(path);
    }

    @Override
    public OfflinePlayer getOfflinePlayer(String path)
    {
        return m_plugin.getConfig().getOfflinePlayer(path);
    }

    @Override
    public OfflinePlayer getOfflinePlayer(String path, OfflinePlayer def)
    {
        return m_plugin.getConfig().getOfflinePlayer(path, def);
    }

    @Override
    public boolean isOfflinePlayer(String path)
    {
        return m_plugin.getConfig().isOfflinePlayer(path);
    }

    @Override
    public ItemStack getItemStack(String path)
    {
        return m_plugin.getConfig().getItemStack(path);
    }

    @Override
    public ItemStack getItemStack(String path, ItemStack def)
    {
        return m_plugin.getConfig().getItemStack(path, def);
    }

    @Override
    public boolean isItemStack(String path)
    {
        return m_plugin.getConfig().isItemStack(path);
    }

    @Override
    public Color getColor(String path)
    {
        return m_plugin.getConfig().getColor(path);
    }

    @Override
    public Color getColor(String path, Color def)
    {
        return m_plugin.getConfig().getColor(path, def);
    }

    @Override
    public boolean isColor(String path)
    {
        return m_plugin.getConfig().isColor(path);
    }

    @Override
    public ConfigurationSection getConfigurationSection(String path)
    {
        return m_plugin.getConfig().getConfigurationSection(path);
    }

    @Override
    public boolean isConfigurationSection(String path)
    {
        return m_plugin.getConfig().isConfigurationSection(path);
    }

    @Override
    public ConfigurationSection getDefaultSection()
    {
        return m_plugin.getConfig().getDefaultSection();
    }

    @Override
    public void addDefault(String path, Object value)
    {
        m_plugin.getConfig().addDefault(path, value);
    }

    @Override
    public void addDefaults(Map<String, Object> defaults)
    {
        m_plugin.getConfig().addDefaults(defaults);
    }

    @Override
    public void addDefaults(Configuration defaults)
    {
        m_plugin.getConfig().addDefaults(defaults);
    }

    @Override
    public void setDefaults(Configuration defaults)
    {
        m_plugin.getConfig().setDefaults(defaults);
    }

    @Override
    public Configuration getDefaults()
    {
        return m_plugin.getConfig().getDefaults();
    }

    @Override
    public ConfigurationOptions options()
    {
        return m_plugin.getConfig().options();
    }

    private final Plugin m_plugin;
    private final Map<String, Entry> m_configEntries;
}


class StringEntry
    implements PluginConfiguration.Entry
{
    StringEntry(PluginConfiguration configuration, String key)
    {
        m_configuration = configuration;
        m_key = key;
    }

    @Override
    public String getValue()
    {
        return m_configuration.getString(m_key);
    }

    @Override
    public void setValue(String value)
    {
        m_configuration.set(m_key, value);
    }

    private final PluginConfiguration m_configuration;
    private final String m_key;
}


class DoubleEntry
    implements PluginConfiguration.Entry
{
    DoubleEntry(PluginConfiguration configuration, String key)
    {
        m_configuration = configuration;
        m_key = key;
    }

    @Override
    public String getValue()
    {
        return String.valueOf(m_configuration.getDouble(m_key));
    }

    @Override
    public void setValue(String value)
    {
        m_configuration.set(m_key, Double.parseDouble(value));
    }

    private final PluginConfiguration m_configuration;
    private final String m_key;
}


class LongEntry
    implements PluginConfiguration.Entry
{
    LongEntry(PluginConfiguration configuration, String key)
    {
        m_configuration = configuration;
        m_key = key;
    }

    @Override
    public String getValue()
    {
        return String.valueOf(m_configuration.getLong(m_key));
    }

    @Override
    public void setValue(String value)
    {
        m_configuration.set(m_key, Long.parseLong(value));
    }

    private final PluginConfiguration m_configuration;
    private final String m_key;
}


class BooleanEntry
    implements PluginConfiguration.Entry
{
    BooleanEntry(PluginConfiguration configuration, String key)
    {
        m_configuration = configuration;
        m_key = key;
    }

    @Override
    public String getValue()
    {
        return String.valueOf(m_configuration.getBoolean(m_key));
    }

    @Override
    public void setValue(String value)
    {
        if(value.equalsIgnoreCase("true"))
        {
            m_configuration.set(m_key, true);
        }
        else if(value.equalsIgnoreCase("false"))
        {
            m_configuration.set(m_key, false);
        }
        else
        {
            throw new NumberFormatException("The value isn't either true or false!");
        }
    }

    private final PluginConfiguration m_configuration;
    private final String m_key;
}