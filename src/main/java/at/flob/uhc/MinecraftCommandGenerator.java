// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


import java.util.Locale;


/**
 * This class provides factory methods to create commands to be executed in Minecraft.
 *
 * @author Florian Graf
 */
class MinecraftCommandGenerator
{
    private static final String SPREAD_PLAYERS_COMMAND = "spreadplayers %.0f %.0f %.0f %.0f %b @a";

    /**
     * Create the spread players command.
     *
     * @param x The center x coordinate.
     * @param z The center z coordinate.
     * @param radius The radius to spread the players.
     * @param minimum The minimum distance there should be between players or teams.
     * @param keepTeams Set to true if teams should be kept together; false otherwise.
     *
     * @return The command without a preceding slash.
     */
    public static String createSpreadPlayersCommand(
        double x,
        double z,
        double radius,
        double minimum,
        boolean keepTeams)
    {
        return createCommand(SPREAD_PLAYERS_COMMAND, x, z, minimum, radius, keepTeams);
    }

    private static String createCommand(String format, Object... arguments)
    {
        return String.format(Locale.ENGLISH, format, arguments);
    }
}
