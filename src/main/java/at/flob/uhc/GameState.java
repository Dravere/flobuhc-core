// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


/**
 * The effective states the UHC game can be in.
 *
 * @author Florian Graf
 */
public enum GameState
{
    /**
     * The plugin is on standby. No UHC game is running but it is possible to create one.
     */
    STANDBY,

    /**
     * An UHC game has been created. At this stage it is possible to configure the parameters for the UHC game.
     */
    CREATED,

    /**
     * The UHC game has been set to ready. Players should have been scattered and everything just be set to go.
     */
    READY,

    /**
     * The UHC game has started and is going on.
     */
    RUNNING,

    /**
     * The UHC game is being finished. It is mostly for the time to review the game before the server returns to normal.
     */
    FINISHED
}
