// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameStateListener;


/**
 * A single or collection of actions to be executed when switching game states.
 *
 * @author Florian Graf
 */
public interface GameStateAction
    extends GameStateListener
{
}
