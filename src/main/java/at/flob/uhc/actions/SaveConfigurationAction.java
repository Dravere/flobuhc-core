// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.PluginConfiguration;


/**
 * @author Florian Graf
 */
public class SaveConfigurationAction
    extends BasicGameStateAction
{
    public SaveConfigurationAction(PluginConfiguration configuration, GameState targetGameState)
    {
        super(targetGameState);

        if(configuration == null)
        {
            throw new IllegalArgumentException("The configuration cannot be null!");
        }

        m_configuration = configuration;
    }

    @Override
    protected void executeAction()
    {
        m_configuration.save();
    }

    private final PluginConfiguration m_configuration;
}
