// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.ServerControl;
import org.bukkit.entity.Player;


/**
 * @author Florian Graf
 */
public class BroadcastMessageAction
    extends BasicGameStateAction
{
    public BroadcastMessageAction(ServerControl serverControl, String message, GameState targetGameState)
    {
        super(targetGameState);

        m_serverControl = serverControl;
        m_message = message;
    }

    @Override
    protected void executeAction()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            player.sendMessage(m_message);
        }
    }

    private final ServerControl m_serverControl;
    private final String m_message;
}
