// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.ServerControl;
import org.bukkit.entity.Player;


/**
 * @author Florian Graf
 */
public class ClearInventoryAction
    extends BasicGameStateAction
{
    public ClearInventoryAction(ServerControl serverControl, GameState targetGameState)
    {
        super(targetGameState);

        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    protected void executeAction()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            player.getInventory().clear();
            player.setExp(0);
        }
    }

    private final ServerControl m_serverControl;
}
