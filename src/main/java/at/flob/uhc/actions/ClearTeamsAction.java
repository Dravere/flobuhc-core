// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.TeamManager;


/**
 * @author Florian Graf
 */
public class ClearTeamsAction
    extends BasicGameStateAction
{
    public ClearTeamsAction(TeamManager teamManager, GameState targetGameState)
    {
        super(targetGameState);

        if(teamManager == null)
        {
            throw new IllegalArgumentException("The team manager cannot be null!");
        }

        m_teamManager = teamManager;
    }

    @Override
    protected void executeAction()
    {
        m_teamManager.clearTeams();
    }

    private final TeamManager m_teamManager;
}
