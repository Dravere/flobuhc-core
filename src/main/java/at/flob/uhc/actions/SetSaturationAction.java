// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.ServerControl;
import org.bukkit.entity.Player;


public class SetSaturationAction
    extends BasicGameStateAction
{
    public SetSaturationAction(ServerControl serverControl, float saturation, GameState targetGameState)
    {
        super(targetGameState);

        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_saturation = saturation;
        m_serverControl = serverControl;
    }

    @Override
    protected void executeAction()
    {
        for(Player player : m_serverControl.getOnlinePlayers())
        {
            player.setSaturation(m_saturation);
        }
    }

    private final float m_saturation;
    private final ServerControl m_serverControl;
}
