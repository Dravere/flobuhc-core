// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import at.flob.uhc.configuration.GameModeConfiguration;
import org.bukkit.entity.Player;


/**
 * @author Florian Graf
 */
public class SpreadPlayerAction
    extends BasicGameStateAction
{
    public SpreadPlayerAction(ServerControl serverControl, GameState targetGameState)
    {
        super(targetGameState);

        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        m_serverControl = serverControl;
    }

    @Override
    protected void executeAction()
    {
        PluginConfiguration configuration = m_serverControl.getConfiguration();

        boolean keepTeams = configuration.getBoolean(PluginConfiguration.GAME_SPREAD_KEEP_TEAMS, false);

        String worldName = configuration.getString(PluginConfiguration.GAME_AREA_PLAY_WORLD, null);
        double centerX = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_CENTER_X, 0);
        double centerZ = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_CENTER_Z, 0);
        double radius = configuration.getDouble(PluginConfiguration.GAME_AREA_PLAY_RADIUS, 1000);
        double minDistance = configuration.getDouble(PluginConfiguration.GAME_SPREAD_MIN_DISTANCE, Double.NaN);

        if(Double.isNaN(minDistance))
        {
            int teamCount = 0;
            if(keepTeams)
            {
                teamCount = m_serverControl.getMainScoreboard().getTeams().size();
            }
            else
            {
                for(Player player : m_serverControl.getOnlinePlayers())
                {
                    if(!GameModeConfiguration.isSpectating(player))
                    {
                        ++teamCount;
                    }
                }
            }

            minDistance = Math.floor(2 * radius / Math.sqrt(teamCount));

            if(radius < minDistance + 1.0)
            {
                minDistance = Math.max(radius - 2, 0);
            }
        }

        m_serverControl.spreadPlayers(worldName, centerX, centerZ, radius, minDistance, keepTeams);
    }

    private final ServerControl m_serverControl;
}
