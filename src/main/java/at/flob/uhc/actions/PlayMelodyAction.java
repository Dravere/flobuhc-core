// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.Melody;
import at.flob.uhc.ServerControl;
import org.bukkit.Instrument;


/**
 * @author Florian Graf
 */
public class PlayMelodyAction
    extends BasicGameStateAction
{
    public PlayMelodyAction(
        ServerControl serverControl,
        Melody melody,
        Instrument instrument,
        GameState targetGameState)
    {
        super(targetGameState);

        if(serverControl == null)
        {
            throw new IllegalArgumentException("The server control cannot be null!");
        }

        if(melody == null)
        {
            throw new IllegalArgumentException("The melody cannot be null!");
        }

        if(instrument == null)
        {
            throw new IllegalArgumentException("The instrument cannot be null!");
        }

        m_melody = melody;
        m_instrument = instrument;
        m_serverControl = serverControl;
    }

    @Override
    protected void executeAction()
    {
        m_melody.play(m_serverControl.getScheduler(), m_serverControl.getOnlinePlayers(), m_instrument);
    }

    private final Melody m_melody;
    private final Instrument m_instrument;
    private final ServerControl m_serverControl;
}
