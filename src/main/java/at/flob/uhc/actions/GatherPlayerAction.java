// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;
import at.flob.uhc.PluginConfiguration;
import at.flob.uhc.ServerControl;
import at.flob.uhc.logging.LoggerFactory;

import java.util.logging.Logger;


/**
 * Gathers the players at (0, 0) using the {@link ServerControl#gatherPlayers(double, double)} method.
 *
 * @author Florian Graf
 */
public class GatherPlayerAction
    extends BasicGameStateAction
{
    private static final Logger s_logger = LoggerFactory.getLogger(GatherPlayerAction.class);

    /**
     * Initializes the action.
     *
     * @param serverControl The control of the current running server.
     * @param targetState The action is executed when the game gets into this state.
     */
    public GatherPlayerAction(ServerControl serverControl, GameState targetState)
    {
        super(targetState);

        m_serverControl = serverControl;
    }

    @Override
    protected void executeAction()
    {
        PluginConfiguration configuration = m_serverControl.getConfiguration();
        String worldName = configuration.getString(PluginConfiguration.GAME_AREA_START_WORLD, null);
        double centerX = configuration.getDouble(PluginConfiguration.GAME_AREA_START_CENTER_X, 0);
        double centerY = configuration.getDouble(PluginConfiguration.GAME_AREA_START_CENTER_Y, Double.NaN);
        double centerZ = configuration.getDouble(PluginConfiguration.GAME_AREA_START_CENTER_Z, 0);

        s_logger.info(() -> "Gathering players at " + centerX + "," + centerZ);
        m_serverControl.gatherPlayers(worldName, centerX, centerY, centerZ);
    }

    private final ServerControl m_serverControl;
}
