// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc.actions;


import at.flob.uhc.GameState;


/**
 * @author Florian Graf
 */
abstract class BasicGameStateAction
    implements GameStateAction
{
    BasicGameStateAction(GameState... targetGameStates)
    {
        if(targetGameStates == null)
        {
            targetGameStates = new GameState[0];
        }

        m_targetGameStates = targetGameStates;
    }

    @Override
    public final void onGameStateChanged(GameState previousState, GameState newState)
    {
        for(GameState gameState : m_targetGameStates)
        {
            if(gameState == newState)
            {
                executeAction();
                return;
            }
        }
    }

    protected abstract void executeAction();

    private final GameState[] m_targetGameStates;
}
