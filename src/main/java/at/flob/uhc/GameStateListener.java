// This file is part of the FLoB UHC core plugin for spigot
// It is distributed under the MIT license
// The source code and license can be found here: https://gitlab.com/Dravere/flobuhc
package at.flob.uhc;


/**
 * A listener for changes of the current game state in the {@link GameStateTracker}.
 *
 * @author Florian Graf
 *
 * @see GameStateTracker
 */
public interface GameStateListener
{
    /**
     * This method is called after the game state changes.
     *
     * @param previousState The previously set game state.
     * @param newState The newly set game state.
     */
    void onGameStateChanged(GameState previousState, GameState newState);
}
